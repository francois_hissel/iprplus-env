/*
 * =====================================================================================
 *
 *       Filename:  calculations.h
 *
 *    Description:  Definition of functions to calculate average parameters from a
 *    							binary file
 *
 *        Version:  1.0
 *        Created:  25/08/2015 10:40:29
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  François Hissel (), francois.hissel@onema.fr
 *   Organization:  Onema
 *
 * =====================================================================================
 */

#ifndef  CALCULATIONS_INC
#define  CALCULATIONS_INC

#include "grid.h"
#include "watershed.h"

extern const char* hydro_modal[];	//!< Labels for hydrological regimes modalities: the labels should match the order of the possible values of this field in the hydrological regime raster map (value 1 in the raster map corresponds to index 0 in this table, value 2 corresponds to index 1...)
extern const char* geol_modal[];	//!< Labels for geological soil modalities: the labels should match the order of the possible values of this field in the geological raster map (value 1 in the raster map corresponds to index 0 in this table, value 2 corresponds to index 1...)

/**
 * \brief Coverage of a raster map
 *
 * This structure basically holds a 2-dimensionnal array of double values and is meant to be used together with a Raster structure of the same dimension. The values in the array are weights that can be used to average a value on the associated Raster.
 */
class Coverage {
	public:
		Coverage():_initialized(false),_weights(0),_total(0.0),_size(0) {}	//!< Standard constructor

		/**
		 * \brief Construct the object based on the associated binary grid and a raster subset
		 *
		 * This function constructs the object and initializes its coverage map, through a call to Coverage::init.
		 * \param data Associated binary grid. The final coverage map will have the same dimension as the this grid, and the values will be placed in the same order.
		 * \param rast Raster subset from which the coverage map is derived
		 */
		Coverage(BinaryGrid *data,RasterSubset *rast);
		Coverage(const Coverage &source);	//!< Copy constructor
		Coverage(Coverage &&source);	//!< Move constructor
		~Coverage();	//!< Standard destructor

		/**
		 * \brief Calculate a coverage map from a binary grid and a raster subset
		 *
		 * This function calculates the coverage map of the rast RasterSubset associated with the data BinaryGrid. The coverage map is stored in the object.
		 * The coverage is a 2-dimension array with the same size of the binary dataset. For each cell of the binary dataset, it associates a double floating-point number giving the area of the raster subset which intersects this cell.
		 * This allows for the calculation of area-weighted averages of the binary dataset over the raster subset. It is used especially for the estimation of the average temperature over a watershed.
		 * \param data Associated binary grid. The final coverage map will have the same dimension as the this grid, and the values will be placed in the same order.
		 * \param rast Raster subset from which the coverage map is derived
		 */
		void init(BinaryGrid *data,RasterSubset *rast);

		bool initialized() const noexcept {return _initialized;};	//!< Accessor to a private variable. Tells if the coverage map has already been calculated.
		double total() const noexcept {return _total;}	//!< Accessor to the sum of weights

		/**
		 * \brief Calculate the dot-product of the coverage map and a binary grid
		 *
		 * This function calculates the scalar product of the coverage map and a binary grid, at a specific time step. This is the same as calculating the weighted average of the binary grid values at this time step, with the weights given by the coverage map. Both objects should have the same dimensions and be ordered in the same way.
		 * When a value is missing in the binary grid (and therefore a fill value was inserted), the function ignores it in the weighted average.
		 * \param grid Operand binary grid variable
		 * \param step Time step
		 * \return Scalar product of coverage map and binary grid or equivalently weighted average of grid values with weights equal to the values of the coverage map. If all values in the binary grid are missing, the function returns NaN.
		 */
		double scalar(const BinaryGrid::Variable &grid,int step) const noexcept;

	private:
		bool _initialized;	//!< Tells if the object was initialized
		double *_weights;	//!< Array of doubles holding the weights. The order of the values should be the same as the order of values in the associated Raster
		double _total;	//!< Sum of weights. It should normally be equal to the total raster subset area, but due to some approximations in the area calculation in order to keep it fast, it may differ from the latter.
		int _size;	//!< Number of values in the array
		int _xmin;	//!< Minimum x-coordinate of the bounding box of non-null values in the coverage grid
		int _ymin;	//!< Minimum y-coordinate of the bounding box of non-null values in the coverage grid
		int _xmax;	//!< Maximum x-coordinate of the bounding box of non-null values in the coverage grid
		int _ymax;	//!< Maximum y-coordinate of the bounding box of non-null values in the coverage grid

		/**
		 * \brief Extend the bounding box of non-null values to include given cell
		 *
		 * This function extends the bounding box of non-null values of the structure to include the cell which coordinates are given as argument. The bounding box is maintained to allow for faster weighted average calculations (in Coverage::scalar).
		 * \param i x-coordinate of new cell
		 * \param j y-coordinate of new cell
		 */
		void extend_min_max(int i,int j) noexcept; 
};

/**
 * \brief Get number of days since January 1st, 1950
 *
 * This function calculates the number of days between January 1st, 1950 and January 1st of the year given as argument.
 * \param year End of the period
 * \return Number of days between the two dates
 */
int get_days(int year);

/**
 * \brief Get an array with the altitudes of the binary mesh cells
 *
 * This function returns a 1D-array with the same size as the main dimension of the binary temperatures dataset read from file temp_file. It first tries to read directly this array from the disk, using the file named filename. If the file is not found, it is computed again. For each cell of the mesh, the function then computes the average altitudes of the corresponding cells in the altitudes GDAL raster file. 
 * \param altitudes GDAL dataset with altitudes
 * \param grid Binary dataset with temperature data. Only the dimensions of the dataset are used to determine the size of the returned array.
 * \param filename Path to a filename with altitudes data. If this file exists, altitudes data is read directly from it. If not, it is created after calculation of the altitudes from the GDAL raster altitudes.
 * \return Array of altitudes of the binary dataset mesh. The array of altitudes is allocated by the function and returned. It is up to the caller to free it with delete[]. The returned array is filled with the longitude (x) varying faster, and the latitudes increasing, as in the NetCDF dataset.
 */
double *get_altitude_mesh(const Raster &altitudes,const BinaryGrid &grid,const char *filename);

/**
 * \brief Get the value of a raster at a point
 *
 * This function gets the integer value of the raster at a selected point. It automatically transforms the geographical coordinates of the point to the coordinates of the raster cell.
 * \param raster Reference of the raster map
 * \param x x-coordinate of the site
 * \param y y-coordinate of the site
 * \return Value at the site according to the raster file
 */
int get_raster_int_value(const Raster &raster,double x,double y);

/**
 * \brief Calculate the average value of data at a point
 *
 * This function calculates the average value of data from the binary dataset variable, at the cell containing the point which coordinates are given. All values from year yearmin to year yearmax are included in the calculation.
 * If altitudes is not null, a temperature correction is applied. In the definition of IPR+, the temperature at a site should be \f$ T_{site}=T_{cell}-(ALT_{site}-ALT{cell})\times 0.0065\f$.
 * \param data Binary dataset variable
 * \param x x-coordinate of the point where the calculation should be made
 * \param y y-coordinate of the point where the calculation should be made
 * \param yearmin Starting year of the period of time for which the calculation should be made (included in the calculation)
 * \param yearmax Ending year of the period of time for which the calculation should be made (included in the calculation)
 * \param altitudes Pointer to a GDAL raster dataset with altitudes, used to apply altitude-based temperature corrections. If null, no correction is applied.
 * \param altitudes_mesh Pointer to an array with altitudes, used to apply altitude-based temperature corrections. This array is returned by the get_altitude_mesh() function.
 * \return Average value of data
 */
double average_point(const BinaryGrid::Variable &data,double x,double y,int yearmin,int yearmax,const Raster *altitudes=0,const double *altitudes_mesh=0);

/**
 * \brief Calculate the average value of a dataset over a watershed
 *
 * This function calculates the average value of data from the binary dataset. The average is weighted with the values read from array coverage. coverage should be the return value of function get_coverage() and represents the coverage of the watershed. All values from year yearmin to year yearmax are included in the calculation.
 * \param data Binary dataset variable
 * \param coverage Coverage map
 * \param yearmin Starting year of the period of time for which the calculation should be made (included in the calculation)
 * \param yearmax Ending year of the period of time for which the calculation should be made (included in the calculation)
 * \return Average value of data
 */
double average_watershed(const BinaryGrid::Variable &data,const Coverage &coverage,int yearmin,int yearmax);

/**
 * \brief Calculate the average temperature amplitude at a point
 *
 * This function calculates the average temperature amplitude, at the cell containing the point which coordinates are given. All values from yearmin to yearmax are included in the calculation.
 * Monthly temperature data is read from the binary grid given as argument. 12 variables are read starting at the pos index.
 * The average temperature amplitude is defined as the difference between the temperature of the hottest and coldest months. Monthly temperatures are averaged over the years.
 * \param data Binary dataset
 * \param pos Index of the variable holding the monthly temperature of January. Following months will be read by incrementing this index.
 * \param coverage Coverage map
 * \param yearmin Starting year of the period of time for which the calculation should be made (included in the calculation)
 * \param yearmax Ending year of the period of time for which the calculation should be made (included in the calculation)
 * \return Average temperature amplitude
 */
double amplitude(const BinaryGrid &data,int pos,double x,double y,int yearmin,int yearmax);

#endif   /* ----- #ifndef CALCULATIONS_INC  ----- */
