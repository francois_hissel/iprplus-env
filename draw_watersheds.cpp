/*! \file iprplus-env.cpp
 * =====================================================================================
 *
 *       Filename:  iprplus-env.cpp
 *
 *    Description:  Calculation of watersheds
 *
 *        Version:  1.0
 *        Created:  11/08/2015 13:53:29
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  François Hissel (), francois.hissel@onema.fr
 *   Organization:  Onema
 *
 * =====================================================================================
 */

// Hack around stdlib bug with C++14.
//#include <initializer_list>  // force libstdc++ to include its config
//#undef _GLIBCXX_HAVE_GETS    // correct broken config
// End hack.
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#ifdef _WIN32
#define	EX_USAGE 64
#define	EX_IOERR 74
#else
#include <sysexits.h>
#endif
#include <exception>
#include <stdexcept>
#include <gdal_priv.h>
#include <ogr_spatialref.h>
#include "watershed.h"

using namespace std;

/**
 * \brief Return a full path for a file
 *
 * This function returns the full path for the file given as argument. If the path does not include a full directory specification, it is considered relative to the path of the executable. Otherwise, it is returned without any change.
 * \param execpath Absolute path of the executable (with the final trailing character)
 * \param path Original path (absolute or relative)
 * \return Full absolute path of the file
 */
string full_path(const string &execpath,const string &path) noexcept {
	if (path.length()==0) return "";
#if defined(_WIN32) || defined(_WIN64)
	if (path[0]=='\\' || (path.length()>1 && path[1]==':')) return path;
	return execpath+path;
#else
	if (path[0]=='/') return path;
	return execpath+path;
#endif
}

/**
 * \brief Display a short help message
 *
 * This function displays a short help message to describe the syntax of the command line.
 */
void display_help() {
	cout << "Syntax: draw_watersheds [options]\n";
	cout << "Options:\n";
	cout << "\t-h      : Show this help message and exit.\n";
	cout << "\t-s file : Set path of GDAL raster file with directions of flow. By default srtm_dir.tif. See documentation for the format of the file.\n";
	cout << "\t-d path : Set a destination directory for output files.\n";
	cout << "\t-e epsg : Set the projection of the coordinates in the input file, by its EPSG code. If 0, no reprojection is done when matching data with NetCDF files (WGS84). Default is 0.\n";
	cout << "\t-f file : Set path of input data. By default, input data is read from standard input.\n";
}

/**
 * \brief Main program
 *
 * This function is the main running loop of the program. It reads arguments from the command line, input data, run the calculations and display the results.
 * The command line has the following syntax: <tt> draw_watersheds [options] </tt>.
 * By default, input data is read from standard input and output is displayed on standard output.
 *
 * Options of the command line are chosen from the following list:
 * 	- <tt>-h</tt> : Display a short help message and exits.
 * 	- <tt>-s file</tt> : Set path of the GDAL raster file with flow directions. By default \a srtm_dir.tif.
 * 	- <tt>-d path</tt> : Set a destination directory for output files.
 * 	- <tt>-e epsg</tt> : Set the projection of the coordinates in the input file, by its EPSG code. If 0, no reprojection is done when matching data with NetCDF files (WGS84). Default is 0.
 * 	- <tt>-f file</tt> : Set path of file with input data. If ommitted, input is read from standard input.
 *
 * 	See also \ref calldoc.
 * 
 * \param argc Number of arguments in the command line, including the path of the program itself
 * \param argv Array of the arguments of the command line
 * \return 0 if the program exitted normally, an error code otherwise
 */
int main(int argc,char **argv) {
	// Process command line arguments
	int i=1;
	bool args_end=false;
	string input_file="";
	string srtm_file="srtm_dir.tif";
	string dest_dir="";
	int epsg=0;
	while (i<argc && !args_end) {
		if (argv[i][0]=='-') {
			switch (argv[i][1]) {
				case 'h':display_help();return 0;
				case 's':case 'f':case 'e':case 'd':
					if (i==argc-1) {
						display_help();
						return EX_USAGE;
					}
					if (argv[i][1]=='s') srtm_file=argv[i+1];
					else if (argv[i][1]=='f') input_file=argv[i+1];
					else if (argv[i][1]=='d') {
						dest_dir=argv[i+1];
						int l=dest_dir.size();
						if (dest_dir[l-1]!='/') dest_dir.append("/");
					}
					else if (argv[i][1]=='e') {
						try {
							epsg=stoi(argv[i+1]);
						} catch (exception &e) {
							cerr << "Syntax error: incorrect EPSG code " << argv[i+1] << ".\n";
						}
					}
					++i;
					break;
				default:
					cerr << "Syntax error: unknown option " << argv[i] << ".\n";
					display_help();
					return EX_USAGE;
			}
			++i;
		} else args_end=true;
	}
	// Get executable path
	string execpath=argv[0];
#if defined(_WIN32) || defined(_WIN64)
	execpath=execpath.substr(0,execpath.find_last_of('\\')+1);
#else
	execpath=execpath.substr(0,execpath.find_last_of('/')+1);
#endif
	// Open input stream
	ifstream ifs;
	if (input_file!="") {
		ifs.open(input_file);
		if (!ifs) {
			cerr << "Impossible to open input file " << input_file << ".\n";
			return EX_IOERR;
		}
		cin.rdbuf(ifs.rdbuf());
		cin.tie(0);
	} 
	// Open flow direction map
	GDALAllRegister();
	Raster rast;
	string path;
	try {
		path=full_path(execpath,srtm_file);
		rast.open(path);
	} catch (exception &e) {
		cerr << "Impossible to open flow direction raster file " << path << ".\n";
		return EX_IOERR;
	}
	// Process input
	double x,y;
	OGRCoordinateTransformation *trans=0;
	if (epsg!=0) {
		OGRSpatialReference src,dest;
		src.importFromEPSG(epsg);
		dest.importFromEPSG(4326);
		trans=OGRCreateCoordinateTransformation(&src,&dest);
	}
	string line,station;
	RasterSubset ss(rast.get_dataset());
	while (!cin.eof()) {
		getline(cin,line);	
		if (line.size()!=0) {
			istringstream iss(line);
			iss.exceptions(ifstream::failbit | ifstream::badbit);
			try {
				iss >> station >> x >> y;
				if (trans!=0) trans->Transform(1,&x,&y);
				ss=rast.watershed(x,y,5e-3);
				ss.save(dest_dir+"bv_"+station+".tif");
				cout << station << "\t-- OK" << endl;
			} catch (exception &e) {
				cout << station << "\t-- Calculation error" << endl;
			}
		}
	}
	if (trans!=0) delete trans;
	// Return and exit
	return 0;
}
