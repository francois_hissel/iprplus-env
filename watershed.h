/*! \file watershed.h
 * =====================================================================================
 *
 *       Filename:  watershed.h
 *
 *    Description:  Library for the calculation of watersheds
 *
 *        Version:  1.0
 *        Created:  11/08/2015 17:15:33
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  François Hissel (), francois.hissel@onema.fr
 *   Organization:  Onema
 *
 * =====================================================================================
 */

#ifndef  WATERSHED_INC
#define  WATERSHED_INC

// Hack around stdlib bug with C++14.
//#include <initializer_list>  // force libstdc++ to include its config
//#undef _GLIBCXX_HAVE_GETS    // correct broken config
// End hack.
#include <string>
#include <exception>
#include <list>
#include <utility>
#include "config.h"
#ifdef  VERBOSE_DEBUG
#include <iostream>
#endif     /* -----  not VERBOSE_DEBUG  ----- */
#include <gdal_priv.h>
#include <ogr_spatialref.h>

typedef int Coord ;

/**
 * \brief Subset of a raster grid
 *
 * This class is used to store a subset of a 2D raster grid. The subset is stored as a grid in a compact form (one bit per cell).
 */
class RasterSubset {
	public:

		/**
		 * \brief Iterator class to go through all elements of the grid, row by row and on each row cell by cell
		 *
		 * This iterator class allows to go through all elements of the RasterSubset object. The iterator is bidirectional. In the normal direction, the iterator advances from left to right on each row, then goes through all rows from top to bottom.
		 * In the reverse direction, it goes from right to left on each row, then through the rows from bottom to top.
		 */
		class HorizontalIterator:public std::iterator<std::bidirectional_iterator_tag,bool> {
			public:
				/**
				 * \brief Constructor of the iterator
				 *
				 * The constructor allows memory for the iterator and initializes its private members. It should normally only be used through a call to RasterSubset::hbegin() or RasterSubset::hrbegin().
				 * \param base Pointer to the base RasterSubset object
				 * \param i Starting column of the iterator in the grid
				 * \param j Starting line of the iterator in the grid
				 */
				HorizontalIterator(const RasterSubset *base,Coord i,Coord j) noexcept;
				HorizontalIterator& operator++() noexcept;	//!< Advance the iterator to the next element in the normal direction
				HorizontalIterator& operator--() noexcept;	//!< Advance the iterator to the next element in the reverse direction
				bool operator==(const HorizontalIterator& hi) noexcept {return _i==hi._i && _j==hi._j;}	//!< Test the equality of two iterators, should only be used if they have the same base class otherwise the result is undefined
				bool operator!=(const HorizontalIterator& hi) noexcept {return _i!=hi._i || _j!=hi._j;}	//!< Test the inequality of two iterators, should only be used if they have the same base class otherwise the result is undefined
				bool operator*() noexcept {return (_base->_data[_i] & _mask)!=0;}	//!< Get the value of the element pointed by the iterator
			private:
				const RasterSubset *_base;	//!< Pointer to the base RasterSubset object
				unsigned char _mask;	//!< Internal mask to get the value of the element pointed by the object
				size_t _i;	//!< Column index of the element pointed by the iterator
				size_t _j;	//!< Row index of the element pointed by the iterator
				Coord _ri;	//!< Column index in the base RasterSubset object of the element pointed by the iterator
		};

		/**
		 * \brief Iterator class to go through all elements of the grid, column by column and on each column cell by cell
		 *
		 * This iterator class allows to go through all elements of the RasterSubset object. The iterator is bidirectional. In the normal direction, the iterator advances from top to bottom on each column, then goes through all columns from left to right.
		 * In the reverse direction, it goes from bottom to top on each column, then through the columns from right to left.
		 */
		class VerticalIterator:public std::iterator<std::bidirectional_iterator_tag,bool> {
			public:
				/**
				 * \brief Constructor of the iterator
				 *
				 * The constructor allows memory for the iterator and initializes its private members. It should normally only be used through a call to RasterSubset::vbegin() or RasterSubset::vrbegin().
				 * \param base Pointer to the base RasterSubset object
				 * \param i Starting column of the iterator in the grid
				 * \param j Starting line of the iterator in the grid
				 */
				VerticalIterator(const RasterSubset *base,Coord i,Coord j) noexcept;
				VerticalIterator& operator++() noexcept;	//!< Advance the iterator to the next element in the normal direction
				VerticalIterator& operator--() noexcept;	//!< Advance the iterator to the next element in the reverse direction
				bool operator==(const VerticalIterator& hi) noexcept {return _i==hi._i && _j==hi._j;}	//!< Test the equality of two iterators, should only be used if they have the same base class otherwise the result is undefined
				bool operator!=(const VerticalIterator& hi) noexcept {return _i!=hi._i || _j!=hi._j;}	//!< Test the inequality of two iterators, should only be used if they have the same base class otherwise the result is undefined
				bool operator*() noexcept {return (_base->_data[_i] & _mask)!=0;}	//!< Get the value of the element pointed by the iterator
			private:
				const RasterSubset *_base;	//!< Pointer to the base RasterSubset object
				unsigned char _mask;	//!< Internal mask to get the value of the element pointed by the object
				size_t _i;	//!< Column index of the element pointed by the iterator
				size_t _j;	//!< Row index of the element pointed by the iterator
				Coord _rj;	//!< Row index in the base RasterSubset object of the element pointed by the iterator
				size_t _w8;	//!< Result of the integral division of _base->_w by 8, stored for better performance
				long _hw8;	//!< Result of the multiplication of _w8 by _base->_h, stored for better performance
		};

		RasterSubset()=default;	//!< Standard constructor

		/**
		 * \brief Constructor of the raster subset
		 *
		 * This constructor allocates memory for the object and initializes its members.
		 * \param width Width of the raster
		 * \param height Height of the raster
		 */
		RasterSubset(Coord width,Coord height);

		/**
		 * \brief Constructor of a raster subset from a source GDAL raster dataset
		 *
		 * This constructor allocates memory for the object and initializes its member to match a source GDAL dataset given as argument.
		 * \param source Pointer to the source GDAL dataset
		 */
		RasterSubset(GDALDataset *source);

		~RasterSubset();	//!< Standard destructor

		/**
		 * \brief Copy constructor
		 *
		 * The constructor copies all members of the source dataset in the newly-constructed one.
		 * \param source Source raster
		 */
		RasterSubset(const RasterSubset &source);

		/**
		 * \brief Move constructor
		 *
		 * The constructor initializes the size of the object to match the source and transfer ownership of the dataset to the newly-constructed one.
		 * \param source Source raster
		 */
		RasterSubset(RasterSubset &&source);

		/**
		 * \brief Move assignment operator
		 *
		 * The operator moves all the member variables from the source object to this one.
		 * \param source Source raster
		 */
		RasterSubset& operator=(RasterSubset &&source);

		/**
		 * \brief Set the value of the pixel at the specified coordinates
		 *
		 * This function sets the value of the pixed at (x,y) to value.
		 * \param x x-coordinate of the cell
		 * \param y y-coordinate of the cell
		 * \param value New value of the pixel
		 */
		void set(Coord x,Coord y,bool value=true) noexcept;

		/**
		 * \brief Simultaneously set the value of multiple contiguous pixels
		 *
		 * This function simultaneously sets the same value for multiple contiguous pixels in the same row. It is better to use than RasterSubset::set for a large number of pixels, as it converts the coordinates only once.
		 */
		void multiset(Coord x1,Coord x2,Coord y,bool value=true) noexcept;

		/**
		 * \brief Get the value of the pixel at the specified coordinates
		 *
		 * This function returns the value of the pixel at the specified coordinates.
		 * \param x x-coordinate of the cell
		 * \param y y-coordinate of the cell
		 * \return Value of the pixel
		 */
		bool get(Coord x,Coord y) const noexcept;

		/**
		 * \brief Save the raster as a GDAL raster file
		 *
		 * This function saves the raster to the file specified as argument, using GDAL library.
		 * \param file Path to the location where the raster file will be saved
		 */
		void save(const std::string &file) const;

		/**
		 * \brief Compute the surface of the raster subset
		 *
		 * This function computes the surface of the raster subset, in square kilometers. It assumes the base raster coordinate system is WGS84 (lat/lon).
		 * \return Area of the raster subset
		 */
		double area() const noexcept;

#ifdef  VERBOSE_DEBUG
		/**
		 * \brief Display the raster subset on an output stream
		 *
		 * This function displays the raster on the output stream. The display contains one character per cell, either 1 if the cell is in the subset, or 0 otherwise.
		 * The output will have the same number of rows and columns as the dimensions of the original raster.
		 */
		void display(std::ostream &out) const noexcept;
#endif     /* -----  not VERBOSE_DEBUG  ----- */

		/**
		 * \brief Accessor function for the width of the base raster layer
		 *
		 * This function returns the width of the underlying raster layer.
		 */
		Coord width() const noexcept {return _worig;}

		/**
		 * \brief Accessor function for the height of the base raster layer
		 *
		 * This function returns the height of the underlying raster layer.
		 */
		Coord height() const noexcept {return _h;}

		/**
		 * \brief Accessor function for the transformation matrix between raster indexes and projection coordinates
		 *
		 * This function writes in its argument a table of six values to describe the transformation matrix between the raster indexes and the projection coordinates.
		 * \param trans After the execution, contains a table of six values describing the transformation matrix. The array must be allocated before the function is called.
		 */
		void get_transformation(double *trans) const noexcept {_ds->GetGeoTransform(trans);}

		HorizontalIterator hbegin() const noexcept {return HorizontalIterator(this,0,0);}
		HorizontalIterator hend() const noexcept {return _hend;}
		HorizontalIterator hrbegin() const noexcept {return HorizontalIterator(this,_worig-1,_h-1);}
		HorizontalIterator hrend() const noexcept {return _hrend;}
		VerticalIterator vbegin() const noexcept {return VerticalIterator(this,0,0);}
		VerticalIterator vend() const noexcept {return _vend;}
		VerticalIterator vrbegin() const noexcept {return VerticalIterator(this,_worig-1,_h-1);}
		VerticalIterator vrend() const noexcept {return _vrend;}

	private:
		GDALDataset *_ds;	//!< GDAL dataset from which this subset is derived, held only to facilitate the saving of a new GDAL file
		Coord _w;	//!< Width of the internal raster grid
		Coord _worig;	//!< Width of the original raster grid
		Coord _h;	//!< Height of the raster
		size_t _n;	//!< Size of the _data structure
		unsigned char *_data;	//!< Data structure which holds the subset. Each cell is represented by 1 bit in the variable. The cells are stored row by row, then column by column, such that the first byte of the array holds the status of the eight first cells in the top row of the grid.

		/**
		 * \brief Calculates the position of a cell in the internal data structure
		 *
		 * This function calculates the position of a specified cell in the _data variable. It returns the offset from the start of the array as well as the position of the bit in the corresponding byte.
		 * \param x x-coordinate of the cell
		 * \param y y-coordinate of the cell
		 * \param[out] i After the execution of the function, first coordinate of the cell in the internal data structure (byte offset)
		 * \param[out] j After the execution of the function, second coordinate of the cell in the internal data structure (bit offset)
		 */
		inline void get_coords(Coord x,Coord y,size_t &i,size_t &j) const noexcept;

		HorizontalIterator _hend;
		HorizontalIterator _hrend;
		VerticalIterator _vend;
		VerticalIterator _vrend;
};

#ifdef  VERBOSE_DEBUG
/**
 * \brief Overload of << operator to allow for the displaying of raster subsets on an output stream
 *
 * This function can be used to display a raster subset on the output stream. It calls the RasterSubset::display method.
 * \param out Output stream
 * \param rast Raster subset to display
 * \return Same output stream as provided, allow for chained outputs
 */
std::ostream &operator<<(std::ostream &out,const RasterSubset &rast);
#endif     /* -----  not VERBOSE_DEBUG  ----- */

/**
 * \brief Raster grid
 *
 * This class holds a raster grid and defines some functions to work on it.
 */
class Raster {
	public:
		Raster():_ds(0),_rb(0),_w(0),_h(0) {}	//!< Standard constructor

		/**
		 * \brief Open a raster file on a newly-created Raster object
		 *
		 * This function creates a raster object and opens a raster file with GDAL. It throws an exception if the file could not be opened.
		 * \param file Path to the raster file
		 */
		Raster(const std::string &file) {open(file);}

		~Raster();	//!< Standard destructor

		/**
		 * \brief Open a raster file
		 *
		 * This function opens a raster file with GDAL. It throws an exception if the file could not be opened.
		 * \param file Path to the raster file
		 */
		void open(const std::string &file);

		/**
		 * \brief Transform projection coordinates to raster grid coordinates
		 *
		 * This function calculates the raster grid coordinates (pixel,line) from the real projection coordinates.
		 * \param x x-coordinate of the projection
		 * \param y y-coordinate of the projection
		 * \return Grid coordinates (pixel,line) of the point
		 */
		std::pair<Coord,Coord> get_coords(double x,double y) const noexcept;

		/**
		 * \brief Calculate the extent of a watershed
		 *
		 * This function calculates the extent of the watershed containing the point which coordinates are given as argument, by applying a quick floodfill algorithm. There is no check whether the coordinates are valid.
		 * \param x First coordinate of the point
		 * \param y Second coordinate of the point
		 * \return Watershed extent
		 */
		RasterSubset watershed(Coord x,Coord y) const;

		/**
		 * \brief Calculate the extent of a watershed
		 *
		 * This function calculates the extent of the watershed containing all the points given as argument, by applying a quick floodfill algorithm. There is no check whether the coordinates are valid.
		 * \param pl List of points coordinates
		 * \return Watershed extent
		 */
		RasterSubset watershed(const std::list<std::pair<Coord,Coord>> &pl) const;

		/**
		 * \brief Calculate the extent of a watershed based on real coordinates of outlet
		 *
		 * This function calculates the extent of the watershed based on the coordinates of the outlet. All points at a given distance of the outlet (less than tolerance) are considered in the watershed.
		 * \param x x-coordinate of the outlet
		 * \param y y-coordinate of the outlet
		 * \param tolerance Distance from the outlet under which all points of the raster are considered in the watershed. The distance is expressed in the same unit as the natural coordinates of the raster. All points within a square which center is the outlet, and which side is twice the tolerance level, will be considered.
		 * \return Watershed extent
		 */
		RasterSubset watershed(double x,double y,double tolerance) const;

		/**
		 * \brief Accessor function for the GDALDataset
		 *
		 * This function returns the internal GDALDataset.
		 * \return GDAL dataset
		 */
		GDALDataset *get_dataset() const noexcept {return _ds;}

		/**
		 * \brief Accessor function for the GDAL raster band
		 *
		 * This function returns the internal GDAL raster band.
		 * \return GDAL raster band
		 */
		GDALRasterBand *get_rasterband() const noexcept {return _rb;}

#ifdef  VERBOSE_DEBUG
		/**
		 * \brief Display the raster on an output stream
		 *
		 * This function displays the raster on the output stream. The display contains one character per cell, with an arrow pointing to the direction of the stream. It is meant to represent the direction of a flow with the same conventions as the SRTM flow direction layer.
		 * The output will have the same number of rows and columns as the dimensions of the original raster.
		 */
		void display(std::ostream &out) const noexcept;
#endif     /* -----  not VERBOSE_DEBUG  ----- */

	private:
		GDALDataset *_ds;	//!< Internal GDAL representation of the raster dataset
		GDALRasterBand *_rb;	//!< Internal GDAL representation of the raster band
		int _w;	//!< Width of the raster
		int _h;	//!< Height of the raster
		double _trans[6];	//!< Transformation matrix between geographical coordinates (lat,lon) and raster coordinates
		double _trans_det;	//!< Determinant of the transformation matrix, stored for performance of point lookups
		OGRCoordinateTransformation *_srs_trans;	//!< Transformation between WGS84 and raster natural SRS 
};

#ifdef  VERBOSE_DEBUG
/**
 * \brief Overload of << operator to allow for the displaying of flow directions raster on an output stream
 *
 * This function can be used to display a flow directions raster file on the output stream. It calls the Raster::display method.
 * \param out Output stream
 * \param rast Raster to display
 * \return Same output stream as provided, allow for chained outputs
 */
std::ostream &operator<<(std::ostream &out,const Raster &rast);
#endif     /* -----  not VERBOSE_DEBUG  ----- */


#endif   /* ----- #ifndef WATERSHED_INC  ----- */
