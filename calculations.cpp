/*! \file calculations.cpp
 * =====================================================================================
 *
 *       Filename:  calculations.cpp
 *
 *    Description:  Implementation of functions to calculate average parameters from
 *    							a grid dataset
 *
 *        Version:  1.0
 *        Created:  25/08/2015 11:03:56
 *       Revision:  none
 *       Compiler:  clang++
 *
 *         Author:  François Hissel (), francois.hissel@onema.fr
 *   Organization:  Onema
 *
 * =====================================================================================
 */

// Hack around stdlib bug with C++14.
//#include <initializer_list>  // force libstdc++ to include its config
//#undef _GLIBCXX_HAVE_GETS    // correct broken config
// End hack.
#include <iostream>
#include <fstream>
#include <cstring>
#include <exception>
#include <limits>
#ifdef  WITH_NETCDF
#include <netcdfcpp.h>
#endif     /* -----  not WITH_NETCDF  ----- */
#include <gdal_priv.h>
#include <ogr_spatialref.h>
#include <ctime>
#include "watershed.h"
#include "grid.h"
#include "calculations.h"

const char* hydro_modal[]={"reg_ng","reg_pn","reg_pm","reg_pf"};
const char* geol_modal[]={"s","c"};

/*
 * =====================================================================================
 *        Support functions
 * =====================================================================================
 */

/**
 * \brief Get index of the value in a sorted array
 *
 * This function searches for index x in the array of values array. It assumes the values are sorted in increasing order. The function returns the index of the value which is just before the searched value in the array.
 * \param array Array of values
 * \param size Size of the array 
 * \param x Value to search
 * \tparam Type of the elements in the array
 * \return Index of the value in the array
 */
template <class T> size_t get_index(const T *array,size_t size,const T &x) {
	size_t j=0;
	while (j<size && array[j]<x) ++j;
	if (j==0) return 0;
	return j-1;
}

int get_days(int year) {
	return (year-1950)*365+(year-1949)/4;
}

/**
 * \brief Compute the area of the intersection between two squares
 *
 * This function computes the area of the intersection between the two squares which coordinates are given.
 * \param xmin1 Lowest x-coordinate of first square
 * \param ymin1 Lowest y-coordinate of first square
 * \param xmax1 Highest x-coordinate of first square
 * \param ymax1 Highest y-coordinate of first square
 * \param xmin2 Lowest x-coordinate of second square
 * \param ymin2 Lowest y-coordinate of second square
 * \param xmax2 Highest x-coordinate of second square
 * \param ymax2 Highest y-coordinate of second square
 * \return Area of the intersection
 */
double intersection(double xmin1,double ymin1,double xmax1,double ymax1,double xmin2,double ymin2,double xmax2,double ymax2) noexcept {
	double xmin,xmax,ymin,ymax;
	if (xmin2<xmin1) xmin=xmin1; else if (xmin2<xmax1) xmin=xmin2; else return 0.0;
	if (xmax2>xmax1) xmax=xmax1; else if (xmax2>xmin1) xmax=xmax2; else return 0.0;
	if (ymin2<ymin1) ymin=ymin1; else if (ymin2<ymax1) ymin=ymin2; else return 0.0;
	if (ymax2>ymax1) ymax=ymax1; else if (ymax2>ymin1) ymax=ymax2; else return 0.0;
	return (xmax-xmin)*(ymax-ymin);
}

/*
 * =====================================================================================
 *        Class Coverage
 * =====================================================================================
 */
Coverage::Coverage(BinaryGrid *data,RasterSubset *rast):_weights(0) {
	init(data,rast);
}

Coverage::~Coverage() {
	delete[] _weights;
	_weights=0;
}

Coverage::Coverage(const Coverage &source):_total(source._total),_size(source._size) {
	_weights=new double[_size];
	std::memcpy(_weights,source._weights,_size*sizeof(double));
}

Coverage::Coverage(Coverage &&source):_weights(source._weights),_total(source._total),_size(source._size) {
	source._weights=0;
}

void Coverage::init(BinaryGrid *data,RasterSubset *rast) {
	delete _weights;
	_xmin=std::numeric_limits<int>::max();
	_xmax=std::numeric_limits<int>::lowest();
	_ymin=std::numeric_limits<int>::max();
	_ymax=std::numeric_limits<int>::lowest();
	int covi=0,covj=0;
	volatile long covk;
	_size=data->sizex()*data->sizey();
	_weights=new double[_size] ();
	double nwx,nwy,swx,swy,nex,ney,interx,intery;
	double trans[6];
	rast->get_transformation(trans);
	bool stop=false;
	bool overx,overy;
	RasterSubset::HorizontalIterator hit=rast->hrbegin();
	int y=rast->height()-1;
	while (!stop && y>=0) {
		int x=rast->width()-1;
		nwx=trans[0]+trans[1]*x+trans[2]*y;
		nwy=trans[3]+trans[4]*x+trans[5]*y;
		swx=nwx+trans[2];
		swy=nwy+trans[5];
		nex=nwx+trans[1];
		ney=nwy+trans[4];
		while (!stop && x>=0) {
			if (*hit) {
				//nwx=trans[0]+trans[1]*x+trans[2]*y;
				//nwy=trans[3]+trans[4]*x+trans[5]*y;
				//swx=nwx+trans[2];
				//swy=nwy+trans[5];
				//nex=nwx+trans[1];
				//ney=nwy+trans[4];
				while (covj<data->sizey()-1 && data->y(covj+1)<=swy) ++covj;
				if (covj>=data->sizey()-1) {
					stop=true;
					break;
				}
				if (covi<data->sizex()-1 && data->x(covi+1)<=swx) covi=data->sizex()-1;
				while (covi>=0 && data->x(covi)>swx) --covi;
				if (covi<0) break;
				if (covi==data->sizex()-1) {
					interx=nex;
					overx=false;
				} else {
					if (data->x(covi+1)>=nex) {
						interx=nex;
						overx=false;
					} else {
						interx=data->x(covi+1);
						overx=true;
					}
				}
				if (covj==data->sizey()-1) {
					intery=ney;
					overy=false;
				} else {
					if (data->y(covj+1)>=ney) {
						intery=ney;
						overy=false;
					} else {
						intery=data->y(covj+1);
						overy=true;
					}
				}
				covk=covj*data->sizex()+covi;
				_weights[covk]+=((interx-swx)*(intery-swy));
				extend_min_max(covi,covj);
				if (overx) {
					_weights[covk+1]+=(nex-interx)*(intery-swy);
					extend_min_max(covi+1,covj);
					if (overy) {
						_weights[covk+data->sizex()]+=(interx-swx)*(ney-intery);
						_weights[covk+data->sizex()+1]+=(nex-interx)*(ney-intery);
						extend_min_max(covi,covj+1);
					}
				} else if (overy) {
					_weights[covk+data->sizex()]+=(interx-swx)*(ney-intery);
					extend_min_max(covi,covj+1);
				}
//				_weights[covj*data->sizex()+covi]+=intersection(data->x(covi),data->y(covj),data->x(covi+1),data->y(covj+1),swx,swy,nex,ney);
//				if (covi>0) _weights[covj*data->sizex()+covi-1]+=intersection(data->x(covi-1),data->y(covj),data->x(covi),data->y(covj+1),swx,swy,nex,ney);
//				if (covj>0) _weights[(covj-1)*data->sizex()+covi]+=intersection(data->x(covi),data->y(covj-1),data->x(covi+1),data->y(covj),swx,swy,nex,ney);
//				if (covi>0 && covj>0) _weights[(covj-1)*data->sizex()+covi-1]+=intersection(data->x(covi-1),data->y(covj-1),data->x(covi),data->y(covj),swx,swy,nex,ney);
			}
			--x;
			nwx-=trans[1];
			nwy-=trans[4];
			swx-=trans[1];
			swy-=trans[4];
			nex-=trans[1];
			nwy-=trans[4];
			--hit;
		}
		--y;
	}
	_total=0.0;
	for (int k=_ymin;k<=_ymax;++k) for (int j=_xmin;j<=_xmax;++j) _total+=_weights[k*data->sizex()+j];
/* 	for (int k=0;k<data->sizey();++k) {
 * 		for (int j=0;j<data->sizex();++j) { 
 * 			std::cerr << _weights[k*data->sizex()+j] << '\t';
 * 		}
 * 		std::cerr << '\n';
 * 	}
 * 	std::cerr << '\n';
 */
	_initialized=true;
}

double Coverage::scalar(const BinaryGrid::Variable &grid,int step) const noexcept {
	int nbval=0;
	int j,k;
	double val=0.0;
	int value;
	double weightsr=_total;
	for (k=_ymin;k<=_ymax;++k) for (j=_xmin;j<=_xmax;++j) {
		value=grid.value(step,j,k);
		if (value!=grid.fillv()) {
			val+=_weights[k*grid.parent().sizex()+j]*(double)value;
			++nbval;
		} else {
			weightsr-=_weights[k*grid.parent().sizex()+j];
		}
	}
	if (nbval!=0) return val/weightsr; 
	return std::numeric_limits<double>::quiet_NaN();
}

void Coverage::extend_min_max(int i,int j) noexcept {
	if (i<_xmin) _xmin=i;
	if (i>_xmax) _xmax=i;
	if (j<_ymin) _ymin=j;
	if (j>_ymax) _ymax=j;
}

/*
 * =====================================================================================
 *        General functions
 * =====================================================================================
 */
double *get_altitude_mesh(const Raster &altitudes,const BinaryGrid &grid,const char *filename) {
	// Try to read from file first
	std::ifstream ifs(filename,std::ios::binary);
	if (ifs) {
		ifs.seekg(0,std::ios::end);
		int size=ifs.tellg();
		ifs.seekg(0,std::ios::beg);
		ifs.exceptions(std::ios::failbit | std::ios::badbit);
		double *buf=new double[size/sizeof(double)];
		ifs.read((char*)(buf),size);
		ifs.close();
		return buf;
	} 
	// If not possible, compute average values
	GDALRasterBand *rb=altitudes.get_rasterband();
	GInt16 nodata=(GInt16)(rb->GetNoDataValue());
	size_t nbvalue;
	double cellx=grid.stepx();
	double celly=grid.stepy();
	double* res=new double[grid.sizex()*grid.sizey()];
	for (int j=0;j<grid.sizey();++j) for (int i=0;i<grid.sizex();++i) {
		std::pair<Coord,Coord> cmin=altitudes.get_coords(grid.x(i)-cellx/2,grid.y(j)-celly/2);
		std::pair<Coord,Coord> cmax=altitudes.get_coords(grid.x(i)+cellx/2,grid.y(j)+celly/2);
		if (std::get<0>(cmin)<0) std::get<0>(cmin)=0;
		if (std::get<1>(cmin)<0) std::get<1>(cmin)=0;
		if (std::get<0>(cmax)<0) std::get<0>(cmax)=0;
		if (std::get<1>(cmax)<0) std::get<1>(cmax)=0;
		if (std::get<0>(cmin)>=rb->GetXSize()) std::get<0>(cmin)=rb->GetXSize()-1;
		if (std::get<1>(cmin)>=rb->GetYSize()) std::get<1>(cmin)=rb->GetYSize()-1;
		if (std::get<0>(cmax)>=rb->GetXSize()) std::get<0>(cmax)=rb->GetXSize()-1;
		if (std::get<1>(cmax)>=rb->GetYSize()) std::get<1>(cmax)=rb->GetYSize()-1;
		long tabsize=(std::get<0>(cmax)-std::get<0>(cmin)+1)*(std::get<1>(cmin)-std::get<1>(cmax)+1);
		GInt16 alt_mesh[tabsize];
		CPLErr err=rb->RasterIO(GF_Read,std::get<0>(cmin),std::get<1>(cmax),std::get<0>(cmax)-std::get<0>(cmin)+1,std::get<1>(cmin)-std::get<1>(cmax)+1,alt_mesh,std::get<0>(cmax)-std::get<0>(cmin)+1,std::get<1>(cmin)-std::get<1>(cmax)+1,GDT_Int16,0,0);
		if (err!=0) continue;
		long sum=0;
		GInt16 *palt=alt_mesh;
		nbvalue=0;
		for (long k=0;k<tabsize;++k) {
			if (*palt!=nodata) {
				sum+=*palt;
				++nbvalue;
			}
			++palt;
		}
		if (nbvalue>0) res[grid.sizex()*j+i]=(double)sum/(double)nbvalue; else res[grid.sizex()*j+i]=0.0;
	}
	// Save resulting array to disk
	std::ofstream ofs(filename,std::ios::binary | std::ios::trunc);
	ofs.write(reinterpret_cast<char*>(res),sizeof(double)*grid.sizex()*grid.sizey());
	ofs.close();
	// Return array
	return res;
}

int get_raster_int_value(const Raster &raster,double x,double y) {
	std::pair<Coord,Coord> c=raster.get_coords(x,y);
	GInt16 val_site;
	GDALRasterBand *rb=raster.get_rasterband();
	CPLErr err=rb->RasterIO(GF_Read,std::get<0>(c),std::get<1>(c),1,1,&val_site,1,1,GDT_Int16,0,0);
	if (err!=CE_None) throw std::exception();
	return val_site;
}

double average_point(const BinaryGrid::Variable &data,double x,double y,int yearmin,int yearmax,const Raster *altitudes,const double *altitudes_mesh) {
	int val=0;
	int nbval=0;
	// Get indices of point
	int ix=data.parent().get_xindex(x);
	int iy=data.parent().get_yindex(y);
	// Get indices of time frame. It is assumed the indices of times are the number of days since 1/1/1950
	int ts=data.parent().get_tindex(yearmin);
	int tf=data.parent().get_tindex(yearmax+1);
	// Calculate average value of data
	int value;
	for (int i=ts;i<tf;++i) {
		value=data.value(i,ix,iy);
		if (value!=data.fillv()) {
			val+=value;
			nbval++;
		}
	}
	if (nbval==0) return 0.0;
	double correction=0.0;
	if (altitudes!=0) correction=-(get_raster_int_value(*altitudes,x,y)-altitudes_mesh[data.parent().sizex()*iy+ix])*0.0065;
	return (double)val/(double)nbval*data.scale()+data.offset()+correction;
}

double average_watershed(const BinaryGrid::Variable &data,const Coverage &coverage,int yearmin,int yearmax) {
	double val=0.0;
	// Get indices of time frame. It is assumed the indices of times are the number of days since 1/1/1950
	int ts=data.parent().get_tindex(yearmin);
	int tf=data.parent().get_tindex(yearmax+1);
	// Calculate average value of data
	double valts;
	int nbval=0;
	for (int i=ts;i<tf;++i) {
		valts=coverage.scalar(data,i);
		if (!isnan(valts)) {
			val+=valts;
			++nbval;
		}
	}
	val/=(double)(nbval);
	return val*data.scale()+data.offset();
}

double amplitude(const BinaryGrid &data,int pos,double x,double y,int yearmin,int yearmax) {
	int val;
	int nbval;
	// Get indices of point
	int ix=data.get_xindex(x);
	int iy=data.get_yindex(y);
	// Get indices of time frame. It is assumed the indices of times are the number of days since 1/1/1950
	int ts=data.get_tindex(yearmin);
	int tf=data.get_tindex(yearmax+1);
	// Calculate average value of data
	double tmon[12];
	int value;
	for (int m=0;m<12;++m) {
		val=0;
		nbval=0;
		for (int i=ts;i<tf;++i) {
			value=data[m+pos].value(i,ix,iy);
			if (value!=data[m+pos].fillv()) {
				val+=value;
				nbval++;
			}
		}
		if (nbval==0) tmon[m]=nan(""); else tmon[m]=(double)val/(double)nbval*data[m+pos].scale()+data[m+pos].offset();
	}
	double min=std::numeric_limits<double>::max();
	double max=std::numeric_limits<double>::lowest();
	for (int m=0;m<12;++m) if (!isnan(tmon[m])) {
		if (min>tmon[m]) min=tmon[m];
		if (max<tmon[m]) max=tmon[m];
	}
	return max-min;
}
