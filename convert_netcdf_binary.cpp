/*
 * =====================================================================================
 *
 *       Filename:  convert_netcdf_binary.cpp
 *
 *    Description:  Convert a NetCDF grid to a binary format suitable for iprplus-env
 *
 *        Version:  1.0
 *        Created:  02/02/2016 20:50:23
 *       Revision:  none
 *       Compiler:  clang++
 *
 *         Author:  François Hissel (), francois.hissel@onema.fr
 *   Organization:  Onema
 *
 * =====================================================================================
 */

#include <iostream>
#include <sstream>
#include <limits>
#include <math.h>
#include "grid.h"

using namespace std;

/****************************************************************
 *                        Class Date                            *
 ***************************************************************/
/**
 * Holds the representation of a date with its year, month and day
 */
class Date {
	public:
		int year;	//!< Year (like 2016...)
		int month;	//!< Index of month in year (0 for January)
		int day;	//!< Index of day in month (0 for first day)

		/**
		 * \brief Constructor of a date from its NetCDF representation
		 *
		 * The constructor allocates memory for the structure and initializes its members based on the date given as argument.
		 * The date argument is represented by a number of days elapsed since 1/1/1950.
		 * \param ndays Date as stored in the NetCDF grids used as input (number of days since 1/1/1950)
		 */
		Date(int ndays);

		Date& operator++();	//!< Prefix incrementation
		Date operator++(int);	//!< Postfix incrementation

		/**
		 * \brief Convert date to NetCDF representation (number of days since 1/1/1950)
		 *
		 * The function returns a NetCDF representation from a Date object. The NetCDF representation is an integer with the number of days elapsed since 1/1/1950. It does the opposite of the constructor.
		 * \return Number of days since 1/1/1950
		 */
		int to_int() const noexcept;	

		constexpr static int mdays[]={31,28,31,30,31,30,31,31,30,31,30,31};	//!< Number of days per month
};
constexpr int Date::mdays[];

Date::Date(int ndays) {
	int total=0;
	year=1950;
	while (total<=ndays) {
		total+=365+((year%4==0)?1:0);
		++year;
	}
	--year;
	total-=(365+((year%4==0)?1:0));
	month=0;
	while (month<12 && total<=ndays) {
		total+=mdays[month]+((month==1 && year%4==0)?1:0);
		++month;
	}
	--month;
	total-=(mdays[month]+((month==1 && year%4==0)?1:0));
	day=ndays-total;
}

Date& Date::operator++() {
	++day;
	if (day>=mdays[month]+((month==1 && year%4==0)?1:0)) {
		++month;
		day=0;
	}
	if (month>=12) {
		++year;
		month=0;
	}
	return *this;
}

Date Date::operator++(int) {
	Date temp=*this;
	++temp;
	return temp;
}

int Date::to_int() const noexcept {
	int val=(year-1950)*365+((year-1948)/4);
	for (int i=0;i<month;++i) val+=mdays[i];
	if (year%4==0 && month>1) ++val;
	return val+day;
}

/****************************************************************
 *                       Main program                           *
 ***************************************************************/
int main(int argc,char **argv) {
	// Load input files
	if (argc<8) {
		cerr << "Syntax: convert_netcdf_binary xmin ymin xmax ymax temp_file prec_file climate_file\n";
		return 1;
	}
	double bounds[4];
	for (int i=0;i<4;++i) bounds[i]=stod(argv[i+1]);
	BinaryGrid temp;
	temp.init_from_netcdf(argv[5],bounds[0],bounds[1],bounds[2],bounds[3]);
	BinaryGrid prec;
	prec.init_from_netcdf(argv[6],bounds[0],bounds[1],bounds[2],bounds[3]);
	cout << temp.sizey() << " lignes, " << temp.sizex() << " colonnes, " << temp.steps() << " pas de temps\n";
	// Calculates start and end date by rounding years
	Date start(temp.time(0));
	if (start.month!=0 || start.day!=0) {
		++start.year;
		start.month=0;
		start.day=0;
	}
	int start_date=start.to_int();
	Date end(temp.time(0)+temp.steps());
	// Initializes tables
	long size=temp.sizex()*temp.sizey()*(end.year-start.year);
	BinaryGrid data(14,temp.sizex(),temp.sizey(),end.year-start.year,temp.x(0),temp.stepx(),temp.y(0),temp.stepy(),start.year,1);
	double *t=new double[size] ();	// Yearly average temperature
	double *p=new double[size] ();	// Yearly cumulated rainfall
	double **tmon=new double*[12];	// Yearly average temperature for each month
	for (int m=0;m<12;++m) tmon[m]=new double[size] ();
	// Populates tables with average values
	for (int i=0;i<temp.sizex();++i) for (int j=0;j<temp.sizey();++j) {	// For each cell in the grid
		int day=start_date-temp.time(0);
		for (int k=start.year;k<end.year;++k) {	// For each year
			int yearlen=365+((k%4==0)?1:0);	// Number of days in year
			int nt=0;
			int np=0;
			long pos=(k-start.year)*temp.sizex()*temp.sizey()+j*temp.sizex()+i;
			for (int m=0;m<12;++m) {	// For each month in year
				int monthlen=Date::mdays[m]+((m==1 && k%4==0)?1:0);	// Number of days in month
				int nmt=0;
				for (int d=0;d<monthlen;++d) {	// For each day in month
					int val=temp[0].value(day,i,j);
					if (val!=temp[0].fillv()) {
						double dval=(double)val*temp[0].scale()+temp[0].offset();
						tmon[m][pos]+=dval;
						t[pos]+=dval;
						++nmt;
						++nt;
					}
					val=prec[0].value(day,i,j);
					if (val!=prec[0].fillv()) {
						double dval=(double)val*prec[0].scale()+prec[0].offset();
						p[pos]+=dval;
						++np;
					}
					++day;
				}
				if (nmt==0) tmon[m][pos]=nan(""); else tmon[m][pos]/=(double)nmt;
			}
			// At this point, taverage holds the sum of temperatures for each month of the year, estimated if some values are missing
			// t and p hold the cumulative temperature and rainfall over the year, with nt and np the number of values which are integrated in the sum
			if (nt==0) t[pos]=nan(""); else t[pos]/=(double)nt;
			if (np==0) p[pos]=nan(""); else {
				if (np<yearlen) p[pos]*=((double)yearlen/(double)np);
			}
		}
	}
	// Initialize grid with arrays
	data[0].init_from_array(t);
	data[1].init_from_array(p);
	for (int m=0;m<12;++m) data[m+2].init_from_array(tmon[m]);
	delete[] t;
	delete[] p;
	for (int m=0;m<12;++m) delete[] tmon[m];
	delete[] tmon;
	// Write to disk and exit
	data.write_to_file(argv[7]);
	data.write_to_text_file("output.log");
/* 	// Load altitudes data
 * 	GDALAllRegister();
 * 	Raster altitudes;
 * 	try {
 * 		altitudes.open(argv[3]);
 * 	} catch (exception &e) {
 * 		cerr << "Impossible to open altitudes raster file " << argv[3] << ".\n";
 * 		return -1;
 * 	}
 * 	// Calculate new grid and write to disk
 * 	BinaryGrid alt=get_altitude_mesh(altitudes,data);
 * 	alt.write_to_file(argv[5]);
 */
	return 0;
}
