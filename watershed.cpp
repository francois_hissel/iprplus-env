/*! \file watershed.cpp
 * =====================================================================================
 *
 *       Filename:  watershed.cpp
 *
 *    Description:  Implementation of functions for the calculation of watersheds
 *
 *        Version:  1.0
 *        Created:  13/08/2015 15:48:23
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  François Hissel (), francois.hissel@onema.fr
 *   Organization:  Onema
 *
 * =====================================================================================
 */

// Hack around stdlib bug with C++14.
#include <initializer_list>  // force libstdc++ to include its config
#undef _GLIBCXX_HAVE_GETS    // correct broken config
// End hack.
#include <stdexcept>
#include <queue>
#include <list>
#include <cstring>
#include <utility>
#include <cpl_port.h>
#include <gdal_priv.h>
#include <ogr_spatialref.h>
#include "config.h"
#ifdef  VERBOSE_DEBUG
#include <iostream>
#endif     /* -----  not VERBOSE_DEBUG  ----- */
#include "watershed.h"

using namespace std;

/*
 * =====================================================================================
 *        Class:  RasterSubset::HorizontalIterator
 * =====================================================================================
 */
RasterSubset::HorizontalIterator::HorizontalIterator(const RasterSubset *base,Coord i,Coord j) noexcept :_base(base),_ri(i) {
	_base->get_coords(i,j,_i,_j);
	_mask=0x80 >> _j;
}

RasterSubset::HorizontalIterator& RasterSubset::HorizontalIterator::operator++() noexcept {
	++_j;
	++_ri;
	if (_j>=8) {
		++_i;
		_j=0;
		_mask=0x80;
	} else _mask>>=1;
	if (_ri>=_base->_worig) {
		_ri=0;
		if (_j!=0) {
			++_i;
			_j=0;
			_mask=0x80;
		}
	}
	return *this;
}

RasterSubset::HorizontalIterator& RasterSubset::HorizontalIterator::operator--() noexcept {
	if (_j==0) {
		if (_i==0) {
			_i=_base->_hrend._i;
			_j=_base->_hrend._j;
			return *this;
		} else {
			--_i;
			if (_ri==0) {
				_ri=_base->_worig-1;
				_j=7-(_base->_w-_base->_worig);
				_mask=1<<(_base->_w-_base->_worig);
			} else {
				--_ri;
				_j=7;
				_mask=1;
			}
		}
	} else {
		--_ri;
		--_j;
		_mask<<=1;
	}
	return *this;
}

/*
 * =====================================================================================
 *        Class:  RasterSubset::VerticalIterator
 * =====================================================================================
 */
RasterSubset::VerticalIterator::VerticalIterator(const RasterSubset *base,Coord i,Coord j) noexcept :_base(base),_rj(j),_w8(_base->_w/8),_hw8(_base->_h*_w8) {
	_base->get_coords(i,j,_i,_j);
	_mask=0x80 >> _j;
}

RasterSubset::VerticalIterator& RasterSubset::VerticalIterator::operator++() noexcept {
	_i+=_w8;
	++_rj;
	if (_rj>=_base->_h) {
		_rj=0;
		_i-=_hw8;
		++_j;
		_mask>>=1;
		if (_j>=8) {
			_j=0;
			_mask=0x80;
			++_i;
		}
	}
	return *this;
}

RasterSubset::VerticalIterator& RasterSubset::VerticalIterator::operator--() noexcept {
	--_rj;
	if (_i>=_w8) _i-=_w8;
	else {
		_i+=(_hw8-_w8);
		if (_j>0) {
			--_j;
			_mask<<=1;
		} else {
			if (_i==0) {
				_i=_base->_vrend._i;
				_j=_base->_vrend._j;
			} else {
				--_i;
				_j=7;
				_mask=1;
			}
		}
	}
	return *this;
}

/*
 * =====================================================================================
 *        Class:  RasterSubset
 * =====================================================================================
 */
RasterSubset::RasterSubset(Coord width,Coord height):_ds(0),_w((width%8==0)?width:(width/8+1)*8),_worig(width),_h(height) ,
		_hend(HorizontalIterator(this,_w,_h-1)),
		_hrend(HorizontalIterator(this,_w,_h)),
		_vend(VerticalIterator(this,_worig,0)),
		_vrend(VerticalIterator(this,_w,_h)) 
{
	_n=((unsigned long long)_w*(unsigned long long)_h)/8+1;
	_data=new unsigned char[_n] ();
}

RasterSubset::RasterSubset(GDALDataset *source):RasterSubset::RasterSubset(source->GetRasterXSize(),source->GetRasterYSize()) {
	_ds=source;
}

RasterSubset::~RasterSubset() {
	delete[] _data;
	_data=0;
}

RasterSubset::RasterSubset(const RasterSubset &source):RasterSubset::RasterSubset(source._worig,source._h) {
	_ds=source._ds;
	memcpy(_data,source._data,_n);
}

RasterSubset::RasterSubset(RasterSubset &&source): _ds(source._ds),_w(source._w),_worig(source._worig),_h(source._h) ,
		_hend(HorizontalIterator(this,_w,_h-1)),
		_hrend(HorizontalIterator(this,_w,_h)),
		_vend(VerticalIterator(this,_worig,0)),
		_vrend(VerticalIterator(this,_w,_h)) 
{
	if (_data!=0) delete[] _data;
	_data=source._data;
	source._data=0;
}

RasterSubset& RasterSubset::operator=(RasterSubset &&source) {
	if (&source==this) return *this;
	_ds=source._ds;
	_w=source._w;
	_worig=source._worig;
	_h=source._h;
	_n=source._n;
	if (_data!=0) delete[] _data;
	_data=source._data;
	source._data=0;
	_hend=source._hend;
	_hrend=source._hrend;
	_vend=source._vend;
	_vrend=source._vrend;
	return *this;
}

inline void RasterSubset::get_coords(Coord x,Coord y,size_t &i,size_t &j) const noexcept {
	unsigned long long v=(unsigned long long)y*(unsigned long long)_w+(unsigned long long)x;
	i=v/8;
	j=v%8;
}

void RasterSubset::set(Coord x,Coord y,bool value) noexcept {
	size_t i,j;
	get_coords(x,y,i,j);
	unsigned char mask=1 << (7-j);
	if (value) _data[i] |= (~mask); else _data[i] &= mask;
}

void RasterSubset::multiset(Coord x1,Coord x2,Coord y,bool value) noexcept {
	size_t i1,i2,j1,j2;
	get_coords(x1,y,i1,j1);
	get_coords(x2,y,i2,j2);
	unsigned char mask;
	if (value) {
		if (i2-i1>=2) memset(_data+i1+1,0xff,i2-i1-1);
		if (i2-i1>=1) {
			mask=(1 << (8-j1))-1;
			_data[i1] |= mask;
			mask=~((1 << (7-j2))-1);
			_data[i2] |= mask;
		} else {
			mask=0;
			for (size_t i=j1;i<=j2;++i) mask|=(1 << (7-i));
			_data[i1] |= mask;
		}
	} else {
		if (i2-i1>=2) memset(_data+i1+1,0,i2-i1-1);
		if (i2-i1>=1) {
			mask=(1 << (8-j1))-1;
			_data[i1] &= (~mask);
			mask=(1 << (7-j2))-1;
			_data[i2] &= mask;
		} else {
			mask=0;
			for (size_t i=j1;i<=j2;++i) mask|=(1 << (7-i));
			_data[i1] &= (~mask);
		}
	}
}

bool RasterSubset::get(Coord x,Coord y) const noexcept {
	size_t i,j;
	get_coords(x,y,i,j);
	unsigned char mask=1 << (7-j);
	return ((_data[i] & mask)!=0);
}

void RasterSubset::save(const string &file) const {
	const char *pszFormat = "GTiff";
	GDALDriver *poDriver;
	poDriver = GetGDALDriverManager()->GetDriverByName(pszFormat);
	if( poDriver == NULL ) exit( 1 );
	GDALDataset *poDstDS;       
	GDALRasterBand *_rb=_ds->GetRasterBand(1);
	char **papszOptions = NULL;
	poDstDS = poDriver->Create(file.c_str(),_w,_h,1,_rb->GetRasterDataType(), papszOptions );
	double adfGeoTransform[6];
	_ds->GetGeoTransform(adfGeoTransform);
	poDstDS->SetGeoTransform(adfGeoTransform);
	const char *pszsrs=_ds->GetProjectionRef();
	GDALRasterBand *poBand;
	poDstDS->SetProjection(pszsrs);
	// Write data
	poBand = poDstDS->GetRasterBand(1);
	char buf[_w];
	for (int i=0;i<_h;++i) {
		for (int j=0;j<_w;++j) buf[j]=get(j,i)?1:0;
		CPLErr err=poBand->RasterIO( GF_Write,0,i,_w,1,buf,_w,1,GDT_Byte,0,0);    
		if (err!=0) continue;
	}
	/* Once we're done, close properly the dataset */
	GDALClose( (GDALDatasetH) poDstDS );
}

#ifdef  VERBOSE_DEBUG
void RasterSubset::display(std::ostream &out) const noexcept {
	for (int y=0;y<_h;++y) {
		for (int x=0;x<_worig;++x) out << (get(x,y)?'1':'0');
		out << '\n';
	}
}

std::ostream &operator<<(std::ostream &out,const RasterSubset &rast) {
	rast.display(out);
	return out;
}
#endif     /* -----  not VERBOSE_DEBUG  ----- */

double RasterSubset::area() const noexcept {
	// Get transformation to allow for the evaluation of coordinates
	double trans[6];
	_ds->GetGeoTransform(trans);
	// Calculate the area of the subset, column by column
	// The area of one pixel is (lon2-lon1)*(sin(lat2)-sin(lat1))*R^2, with R the radius of the Earth
	double res=0.0;
	const double conv=3.141592653589793/180.0;
	VerticalIterator it=vbegin();
	for (int x=0;x<_w;++x) {
		int y1=0;
		int y2;
		while (y1<_h) {
			while (y1<_h && !(*it)) {
				++y1;
				++it;
			}
			if (y1<_h) {
				y2=y1+1;
				++it;
				while (y2<_h && *it) {
					++y2;
					++it;
				}
				res+=(sin(conv*(trans[3]+x*trans[4]+y1*trans[5]))-sin(conv*(trans[3]+x*trans[4]+y2*trans[5])));
				y1=y2+1;
				++it;
			}
		}
	}
	return res*trans[1]*conv*6378.137*6378.137;
}

/*
 * =====================================================================================
 *        Class:  Raster
 * =====================================================================================
 */
void Raster::open(const string &file) {
	_ds=(GDALDataset*)GDALOpen(file.c_str(),GA_ReadOnly);
	if (_ds==nullptr) throw std::exception();
	_rb=_ds->GetRasterBand(1);
	_w=_rb->GetXSize();
	_h=_rb->GetYSize();
	_ds->GetGeoTransform(_trans);
	_trans_det=_trans[1]*_trans[5]-_trans[2]*_trans[4];
	OGRSpatialReference src,dest;
	src.importFromEPSG(4326);
	dest=OGRSpatialReference(_ds->GetProjectionRef());
#if GDAL_MAJOR_VERSION >= 3
	src.SetAxisMappingStrategy(OAMS_TRADITIONAL_GIS_ORDER);
	dest.SetAxisMappingStrategy(OAMS_TRADITIONAL_GIS_ORDER);
#endif
	_srs_trans=OGRCreateCoordinateTransformation(&src,&dest);
}

Raster::~Raster() {
	if (_ds!=0) GDALClose((GDALDatasetH)(_ds));
	if (_srs_trans!=0) delete _srs_trans;
}

/**
 * \brief Structure holding a search interval of a scanline in the floodfill algorithm
 *
 * This structure holds the coordinates of a search interval in the floodfill algorithm.
 */
struct ScanInterval {
	Coord x1;	//!< Left x-coordinate of the search interval (included in the interval)
	Coord x2;	//!< Right x-coordinate of the search interval (included in the interval)
	Coord y;	//!< Y-coordinate of scanline
};

std::pair<Coord,Coord> Raster::get_coords(double x,double y) const noexcept {
	_srs_trans->Transform(1,&x,&y);
	double xx=x-_trans[0];
	double yy=y-_trans[3];
	return std::make_pair((Coord)((_trans[5]*xx-_trans[2]*yy)/_trans_det),(Coord)((-_trans[4]*xx+_trans[1]*yy)/_trans_det));
}


RasterSubset Raster::watershed(const list<pair<Coord,Coord>> &pl) const {
	RasterSubset res(_ds);
	Coord minx,maxx,x,y;
	CPLErr err;
	vector<GInt16> dat(_w);
	std::queue<ScanInterval,std::list<ScanInterval>> que;
	for (auto &it:pl) {
		x=std::get<0>(it);
		y=std::get<1>(it);
		err=_rb->RasterIO(GF_Read,0,y,_w,1,dat.data(),_w,1,GDT_Int16,0,0);
		//err=_rb->ReadBlock(0,y,dat.data());
		if (err!=CE_None) return res;
		if (!res.get(x,y)) {
			minx=x;maxx=x;
			do { --minx; } while (minx>=0 && !res.get(minx,y) && dat[minx]==1); 
			do { ++maxx; } while (maxx<_w && !res.get(maxx,y) && dat[maxx]==16); 
			++minx;
			--maxx;
			res.multiset(minx,maxx,y);
			que.emplace(ScanInterval{minx,maxx,y});
		}
	}
	while (!que.empty()) {
		ScanInterval inter=que.front();
		que.pop();
		//cerr << '\n' << res;
		//cerr << "* Intervalle (" << inter.x1 << "," << inter.x2 << "," << inter.y << ")\n";
		if (inter.y>0) {
			y=inter.y-1;
			x=inter.x1;
			//cerr << "\tx=" << x << ",y=" << y << "\n";
			minx=(x>0)?(x-1):0; 
			err=_rb->RasterIO(GF_Read,0,y,_w,1,dat.data(),_w,1,GDT_Int16,0,0);
			//err=_rb->ReadBlock(0,y,dat.data());
			if (err!=CE_None) return res;
			while (minx<_w && minx<=inter.x2+1 && (res.get(minx,y) || ((dat[minx]!=4 || minx>inter.x2 || minx<inter.x1) && (dat[minx]!=2 || minx>inter.x2-1) && (dat[minx]!=8 || minx<inter.x1+1)))) ++minx;
			//cerr << "\tAvant   : minx=" << minx << "\n";
			if (minx<=inter.x2+1 && minx<_w) {
				do {--minx;} while (minx>=0 && !res.get(minx,y) && (dat[minx]==1 || (dat[minx]==2 && minx>=inter.x1-1)));
				++minx;
				//cerr << "\tArrière : minx=" << minx << "\n";
				x=minx+1;
				while (x<=inter.x2+2 && minx<_w) {
					//cerr << "\tBoucle : minx=" << minx << ",x=" << x << "\n";
					maxx=x-1;
					while (x<_w && !res.get(x,y) && (dat[x]==16 || dat[x]==1 || (dat[x]==8 && x<=inter.x2+1) || (dat[x]==4 && x<=inter.x2) || (dat[x]==2 && x<=inter.x2-1))) {
						if (dat[x]!=1) maxx=x;
						++x;
					}
					if (minx<=maxx) {
						//cerr << "\tSet (" << minx << "," << maxx << "," << y << ")\n";
						que.emplace(ScanInterval{minx,maxx,y});
						res.multiset(minx,maxx,y);
					}
					minx=x;
					while (minx<_w && minx<=inter.x2+1 && (res.get(minx,y) || ((dat[minx]!=4 || minx>inter.x2) && (dat[minx]!=2 || minx>inter.x2-1) && (dat[minx]!=8)))) ++minx;
					do {--minx;} while (minx>=x && !res.get(minx,y) && (dat[minx]==1 || (dat[minx]==2 && minx>=inter.x1-1)));
					++minx;
					x=minx+1;
				}
			}
		}
		if (inter.y<_h-1) {
			y=inter.y+1;
			x=inter.x1;
			//cerr << "\tx=" << x << ",y=" << y << "\n";
			minx=(x>0)?(x-1):0; maxx=x;
			err=_rb->RasterIO(GF_Read,0,y,_w,1,dat.data(),_w,1,GDT_Int16,0,0);
			//err=_rb->ReadBlock(0,y,dat.data());
			if (err!=CE_None) return res;
			while (minx<_w && minx<=inter.x2+1 && (res.get(minx,y) || ((dat[minx]!=64 || minx>inter.x2 || minx<inter.x1) && (dat[minx]!=128 || minx>inter.x2-1) && (dat[minx]!=32 || minx<inter.x1+1)))) ++minx;
			//cerr << "\tAvant   : minx=" << minx << "\n";
			if (minx<=inter.x2+1 && minx<_w) {
				do {--minx;} while (minx>=0 && !res.get(minx,y) && (dat[minx]==1 || (dat[minx]==128 && minx>=inter.x1-1)));
				++minx;
				//cerr << "\tArrière : minx=" << minx << "\n";
				x=minx+1;
				while (x<=inter.x2+2 && minx<_w) {
					//cerr << "\tBoucle : minx=" << minx << ",x=" << x << "\n";
					maxx=x-1;
					while (x<_w && !res.get(x,y) && (dat[x]==16 || dat[x]==1 || (dat[x]==32 && x<=inter.x2+1) || (dat[x]==64 && x<=inter.x2) || (dat[x]==128 && x<=inter.x2-1))) {
						if (dat[x]!=1) maxx=x;
						++x;
					}
					if (minx<=maxx) {
						//cerr << "\tSet (" << minx << "," << maxx << "," << y << ")\n";
						que.emplace(ScanInterval{minx,maxx,y});
						res.multiset(minx,maxx,y);
					}
					minx=x;
					while (minx<_w && minx<=inter.x2+1 && (res.get(minx,y) || ((dat[minx]!=64 || minx>inter.x2) && (dat[minx]!=128 || minx>inter.x2-1) && (dat[minx]!=32)))) ++minx;
					do {--minx;} while (minx>=x && !res.get(minx,y) && (dat[minx]==1 || (dat[minx]==128 && minx>=inter.x1-1)));
					++minx;
					x=minx+1;
				}
			}
		}
	} 
	return res;
}

RasterSubset Raster::watershed(Coord x,Coord y) const {
	return watershed(list<pair<Coord,Coord>>(1,std::make_pair(x,y)));
}

RasterSubset Raster::watershed(double x,double y,double tolerance) const {
	pair<Coord,Coord> u=get_coords(x-tolerance,y-tolerance);
	pair<Coord,Coord> v=get_coords(x+tolerance,y+tolerance);
	list<pair<Coord,Coord>> li;
	for (Coord i=get<0>(u);i<=get<0>(v);++i) for (Coord j=get<1>(v);j<=get<1>(u);++j) li.push_back(std::make_pair(i,j));
	return watershed(li);
}

#ifdef  VERBOSE_DEBUG
void Raster::display(ostream &out) const noexcept {
	vector<GInt16> dat(_w);
	for (int y=0;y<_h;++y) {
		CPLErr err=_rb->RasterIO(GF_Read,0,y,_w,1,dat.data(),_w,1,GDT_Int16,0,0);
		if (err!=CE_None) throw std::exception();
		for (int x=0;x<_w;++x) {
			switch (dat[x]) {
				case 0:out << '5';break;
				case 1:out << '6';break;
				case 2:out << '3';break;
				case 4:out << '2';break;
				case 8:out << '1';break;
				case 16:out << '4';break;
				case 32:out << '7';break;
				case 64:out << '8';break;
				case 128:out << '9';break;
			}
		}
		out << '\n';
	}
}

std::ostream &operator<<(std::ostream &out,const Raster &rast) {
	rast.display(out);
	return out;
}
#endif     /* -----  not VERBOSE_DEBUG  ----- */
