/*
 * =====================================================================================
 *
 *       Filename:  grid.cpp
 *
 *    Description:  Implementation of functions for reading and writing gridded data 
 *
 *        Version:  1.0
 *        Created:  02/02/2016 11:08:44
 *       Revision:  none
 *       Compiler:  clang++
 *
 *         Author:  François Hissel (), francois.hissel@onema.fr
 *   Organization:  Onema
 *
 * =====================================================================================
 */

#include <cstring>
#include <cstdint>
#include <math.h>
#include <utility>
#include <limits>
#include <fstream>
#include <exception>
#include "config.h"
#ifdef  WITH_NETCDF
#include <netcdfcpp.h>
#endif     /* -----  not WITH_NETCDF  ----- */
#include "grid.h"

using namespace std;

/*********************************************************
 *                  Support functions                    *
 ********************************************************/
/**
 * \brief Get index of the value in a sorted array
 *
 * This function searches for index x in the array of values array. It assumes the values are sorted in increasing order. The function returns the index of the value which is just before the searched value in the array.
 * \tparam T Type of the value in the array
 * \param array Array of values
 * \param size Size of the array 
 * \param x Value to search
 * \tparam Type of the elements in the array
 * \return Index of the value in the array
 */
template <typename T> size_t get_index(const T *array,size_t size,const T &x) {
	size_t j=0;
	while (j<size && array[j]<x) ++j;
	if (j==0) return 0;
	return j-1;
}

/**
 * \brief Convert a little-endian representation to this platform
 *
 * This function converts a little-endian representation of a variable to the format of the platform where this code is compiled. The detection of endianness is made at compile time, so the code should be recompiled when ported to another platform.
 * \tparam T Type of the value
 * \param val Representation of the value in little-endian format
 * \return Real value for this platform
 */
template <typename T> inline T from_little_endian(const T& val) {
#ifdef  BIGENDIAN
	unsigned char buf[sizeof(T)];
	memcpy(buf,(void*)(&val),sizeof(T));
	size_t l=sizeof(T);
	size_t s=l/2;
	for (size_t i=0;i<s;++i) swap(buf[i],buf[l-1-i]);
	return reinterpret_cast<T>(buf);
#else      /* -----  not BIGENDIAN  ----- */
	return val;
#endif     /* -----  not BIGENDIAN  ----- */
}

/**
 * \brief Convert a locale platform representation to little-endian
 *
 * This function converts a locale platform representation of a variable to a little-endian representation. The detection of endianness is made at compile time, so the code should be recompiled when ported to another platform.
 * \tparam T Type of the value
 * \param val Representation of the value in the format of the platform where this code is compiled
 * \return Little endian representation of the value
 */
template <typename T> inline T to_little_endian(const T& val) {
	return from_little_endian(val);	// The transformation is actually its own inverse. However, the function is kept to support good programming practices
}

/**
 * \brief Get a value from a binary stream in a portable way
 *
 * This function reads a value of type T from the stream given as argument. The value read is considered as a little-endian value and converted to locale endianness if needed.
 * \tparam T Type of the value
 * \param ifs Input stream, should be ready for reading
 * \return Value of type T read from the stream
 */
template <typename T> T read_binary(istream &ifs) {
	T val;
	ifs.read((char*)(&val),sizeof(T));
	return from_little_endian(val);
}

/**
 * \brief Write a value to a binary stream in a portable way
 *
 * This function writes a value of type T to the stream given as argument. The value is written as little-endian and converted to this convention if needed.
 * \tparam T Type of the value
 * \param ofs Output stream, should be ready for writing
 * \param value Integer value to be written to the stream
 */
template <typename T> void write_binary(ostream &ofs,T value) {
	T val=to_little_endian(value);
	ofs.write((char*)(&val),sizeof(T));
}

/*********************************************************
 *                Class BinaryGrid::Variable             *
 ********************************************************/
BinaryGrid::Variable::Variable(const BinaryGrid::Variable &source):_parent(source._parent),_fillv(source._fillv),_scale(source._scale),_offset(source._offset) {
  long size=_parent->_steps*_parent->_sizexy;
	_buffer=new int16_t[size];
	memcpy(_buffer,source._buffer,size*sizeof(int16_t));
}

BinaryGrid::Variable::Variable(BinaryGrid::Variable &&source):_parent(source._parent),_fillv(source._fillv),_scale(source._scale),_offset(source._offset) {
}

BinaryGrid::Variable::Variable(BinaryGrid *parent):_parent(parent) {
	if (parent!=0) _buffer=new int16_t[_parent->_steps*_parent->_sizexy];
}

BinaryGrid::Variable::~Variable() {
	delete[] _buffer;
	_buffer=0;
}

void BinaryGrid::Variable::set_parent(BinaryGrid *parent) {
	delete[] _buffer;
	_parent=parent;
	if (parent!=0) _buffer=new int16_t[_parent->_steps*_parent->_sizexy];
}

void BinaryGrid::Variable::write(ostream &os) const {
	// Write general values
	write_binary<int16_t>(os,_fillv);
	write_binary<double>(os,_scale);
	write_binary<double>(os,_offset);
	// Write data
	int16_t *buf=_buffer;
	long size=_parent->_steps*_parent->_sizexy;
	for (long i=0;i<size;++i) write_binary<int16_t>(os,*(buf++));
}

void BinaryGrid::Variable::write_text(std::ostream &os) const {
	os << _fillv << '\t' << _scale << '\t' << _offset << '\n';
	for (int i=0;i<_parent->_steps;++i) {
		for (int j=0;j<_parent->_sizey;++j) {
			for (int k=0;k<_parent->_sizex;++k) {
				os << real_value(i,k,j) << '\t';
			}
			os << '\n';
		}
		os << '\n';
	}
}

void BinaryGrid::Variable::init_from_array(const double *dvalues) {
	delete[] _buffer;
	long size=_parent->_sizexy*_parent->_steps;
	_buffer=new int16_t[size];
	// Initialize private variables
	_fillv=-32768;
	// Compute scale value and offset
	double min=std::numeric_limits<double>::max();
	double max=std::numeric_limits<double>::lowest();
	const double *dp=dvalues;
	for (long i=0;i<size;++i,++dp) {
		if (!isnan(*dp)) {
			if (*dp<min) min=*dp;
			if (*dp>max) max=*dp;
		}
	}
	_offset=(max+min)/2.0;
	_scale=(max-_offset)/32767.0;
	// Fill array values
	dp=dvalues;
	int16_t *bufp=_buffer;
	for (long i=0;i<size;++i,++dp,++bufp) {
		if (!isnan(*dp)) *bufp=lround((*dp-_offset)/_scale); else *bufp=_fillv;
	}
}
/*********************************************************
 *                    Class BinaryGrid                   *
 ********************************************************/
BinaryGrid::BinaryGrid(const BinaryGrid& source):_nvars(source._nvars),_sizex(source._sizex),_sizey(source._sizey),_sizexy(_sizex*_sizey),_steps(source._steps),_startx(source._startx),_stepx(source._stepx),_starty(source._starty),_stepy(source._stepy),_start_time(source._start_time),_step_time(source._step_time),_vars(source._vars) {
}

BinaryGrid::BinaryGrid(BinaryGrid&& source):_nvars(source._nvars),_sizex(source._sizex),_sizey(source._sizey),_sizexy(_sizex*_sizey),_steps(source._steps),_startx(source._startx),_stepx(source._stepx),_starty(source._starty),_stepy(source._stepy),_start_time(source._start_time),_step_time(source._step_time),_vars(source._vars) {
	source._vars=0;
}

BinaryGrid::~BinaryGrid() {
	delete[] _vars;
	_vars=0;
}

BinaryGrid::BinaryGrid(int nvars,int sizex,int sizey,int steps,double xmin,double xstep,double ymin,double ystep,int tmin,int tstep):_nvars(nvars),_sizex(sizex),_sizey(sizey),_sizexy(_sizex*_sizey),_steps(steps),_startx(xmin),_stepx(xstep),_starty(ymin),_stepy(ystep),_start_time(tmin),_step_time(tstep) {
	_vars=new Variable[_nvars];
	for (int i=0;i<nvars;++i) _vars[i].set_parent(this);
}

int BinaryGrid::get_xindex(double x) const noexcept {
	int v=(int)((x-_startx)/_stepx);
	if (v<=0) return 0;
	if (v>=_sizex) return _sizex-1;
	return v;
}

int BinaryGrid::get_yindex(double y) const noexcept {
	int v=(int)((y-_starty)/_stepy);
	if (v<=0) return 0;
	if (v>=_sizey) return _sizey-1;
	return v;
}

int BinaryGrid::get_tindex(int t) const noexcept {
	int v=(int)((t-_start_time)/_step_time);
	if (v<=0) return 0;
	if (v>=_steps) return _steps-1;
	return v;
}

void BinaryGrid::write_to_file(const char *file) const {
	ofstream ofs(file,ofstream::out | ofstream::binary);
	if (!ofs) throw std::exception();
	// Write general values
	write_binary<uint16_t>(ofs,_nvars);
	write_binary<uint16_t>(ofs,_sizex);
	write_binary<uint16_t>(ofs,_sizey);
	write_binary<uint32_t>(ofs,_steps);
	write_binary<double>(ofs,_startx);
	write_binary<double>(ofs,_stepx);
	write_binary<double>(ofs,_starty);
	write_binary<double>(ofs,_stepy);
	write_binary<int32_t>(ofs,_start_time);
	write_binary<int16_t>(ofs,_step_time);
	// Write variables data
	for (int i=0;i<_nvars;++i) _vars[i].write(ofs);
}

void BinaryGrid::write_to_text_file(const char *file) const {
	ofstream ofs(file,ofstream::out | ofstream::binary);
	if (!ofs) throw std::exception();
	// Write general values
	ofs << _nvars << '\t' << _sizex << '\t' << _sizey << '\t' << _steps << '\t' << _startx << '\t' << _stepx << '\t' << _starty << '\t' << _stepy << '\t' << _start_time << '\t' << _step_time << '\n';
	// Write variables data
	for (int i=0;i<_nvars;++i) {
		ofs << "\nVariable " << i << "\n";
		_vars[i].write_text(ofs);
	}
}

#ifdef  WITH_NETCDF
void BinaryGrid::init_from_netcdf(const char *file,double x1,double y1,double x2,double y2) {
	NcFile _file(file);
	if (!_file.is_valid()) throw std::exception();
	// Get longitude and latitude dimensions
	NcVar *_varx=_file.get_var("longitude");
	NcVar *_vary=_file.get_var("latitude");
	NcVar *_vart=_file.get_var("time");
	_sizex=_varx->num_vals();
	_sizey=_vary->num_vals();
	_sizexy=_sizex*_sizey;
	_steps=_vart->num_vals();
	double _x[2];
	if (!_varx->get(_x,2)) throw std::exception();
	_startx=(3*_x[0]-_x[1])/2;	// By convention, NetCdf coordinates are located at the center of the cell. The program assumes the coordinates are at the top-left corner, so we have to shift.
	_stepx=_x[1]-_x[0];
	if (!_vary->get(_x,2)) throw std::exception();
	_starty=(3*_x[0]-_x[1])/2;	// Same remark as above
	_stepy=_x[1]-_x[0];
	int _t[2];
	if (!_vart->get(_t,2)) throw std::exception();
	_start_time=_t[0];
	_step_time=_t[1]-_t[0];
	// Calculate the dimensions of the extracted area
	int xmin=(int)((x1-_startx)/_stepx);
	int xmax=(int)((x2-_startx)/_stepx);
	int ymin=(int)((y1-_starty)/_stepy);
	int ymax=(int)((y2-_starty)/_stepy);
	_startx+=(_stepx*xmin);
	_starty+=(_stepy*ymin);
	_sizex=(xmax-xmin)+1;
	_sizey=(ymax-ymin)+1;
	_sizexy=_sizex*_sizey;
	// Get main data variable
	int j=_file.num_vars();
	int i=0;
	while (i<j && _file.get_var(i)->num_dims()<=1) ++i;
	if (i>=j) throw std::exception();
	NcVar *_data=_file.get_var(i);
	delete[] _vars;
	_nvars=1;
	_vars=new BinaryGrid::Variable[1]{this};
	Variable &variable=_vars[0];
	// Read some major attributes
	variable._scale=1.0;
	variable._offset=0.0;
	variable._fillv=std::numeric_limits<int16_t>::lowest();
	NcAtt *attr;
	j=_data->num_atts();
	for (i=0;i<j;++i) {
		attr=_data->get_att(i);
		if (strcmp(attr->name(),"scale_factor")==0) variable._scale=attr->as_double(0);	// Get scale factor of main data variable
		else if (strcmp(attr->name(),"add_offset")==0) variable._offset=attr->as_double(0);	// Get offset of main data variable
		else if (strcmp(attr->name(),"_FillValue")==0) variable._fillv=attr->as_int(0);	// Get fill value of main data variable
		delete attr;
		attr=0;
	}
	// Read data
	if (!_data->set_cur(0,ymin,xmin) || !_data->get(variable._buffer,_steps,_sizey,_sizex)) throw std::exception();
}
#endif     /* -----  not WITH_NETCDF  ----- */

void BinaryGrid::init(const char *file) {
	// Open file
	ifstream ifs(file,ifstream::binary | ifstream::in);
	if (!ifs) throw std::exception();
	ifs.exceptions(ifstream::eofbit | ifstream::failbit | ifstream::badbit);
	// Read general values
	_nvars=read_binary<uint16_t>(ifs);
	_sizex=read_binary<uint16_t>(ifs);
	_sizey=read_binary<uint16_t>(ifs);
	_sizexy=_sizex*_sizey;
	_steps=read_binary<uint32_t>(ifs);
	_startx=read_binary<double>(ifs);
	_stepx=read_binary<double>(ifs);
	_starty=read_binary<double>(ifs);
	_stepy=read_binary<double>(ifs);
	_start_time=read_binary<int32_t>(ifs);
	_step_time=read_binary<int16_t>(ifs);
	// Read data
	delete[] _vars;
	_vars=new BinaryGrid::Variable[_nvars];
	for (int i=0;i<_nvars;++i) {
		Variable &variable=_vars[i];
		variable.set_parent(this);
		int16_t *buf=variable._buffer;
		variable._fillv=read_binary<int16_t>(ifs);
		variable._scale=read_binary<double>(ifs);
		variable._offset=read_binary<double>(ifs);
		long size=_steps*_sizexy;
		for (long i=0;i<size;++i) *(buf++)=read_binary<int16_t>(ifs);
	}
}

