/*
 * =====================================================================================
 *
 *       Filename:  grid.h
 *
 *    Description:  Definition of functions for reading and writing gridded data
 *
 *        Version:  1.0
 *        Created:  01/02/2016 12:56:34
 *       Revision:  none
 *       Compiler:  clang++
 *
 *         Author:  François Hissel (), francois.hissel@onema.fr
 *   Organization:  Onema
 *
 * =====================================================================================
 */

#ifndef  GRID_INC
#define  GRID_INC

#include "config.h"

/**
 * \brief Gridded data structure
 *
 * This abstract class defines the basic interface of a gridded data structure. For the needs of this program, a gridded data structure is the internal representation of a four-dimensional array used to store spatial raster data varying with time.
 * A grid may hold one or several variables. For each variable, the first dimension is the time, the second and third dimensions are spatial dimensions.
 * The gridded data structure is stored in a compact format: every cell is a 16-bits signed integer, and each variable structure holds a scaling coefficient and an offset to map the integer values to the real values. This allows for faster calculations.
 */
class BinaryGrid {
	public:
		/**
		 * \brief Variable of a multi-variable grid
		 *
		 * This class represents one variable in a grid holding several variables. A variable is described by a fill value, a scale coefficient, an offset and an array of 16-bits signed integers.
		 */
		class Variable {
			public:
				friend class BinaryGrid;
				Variable():_parent(0),_buffer(0) {}	//!< Standard constructor
				Variable(BinaryGrid *parent);	//!< Standard constructor with reference to parent

				/**
				 * \brief Copy constructor
				 *
				 * The copy constructor copies all members to the new structure.
				 *
				 * \param source Source object
				 */
				Variable(const Variable &source);

				/**
				 * \brief Move constructor
				 *
				 * The move constructor copies small members to the new object and transfers ownership of larger members.
				 *
				 * \param source Source object
				 */
				Variable(Variable&& source);

				~Variable();	//!< Standard destructor

				int16_t fillv() const noexcept {return _fillv;}	//!< Accessor to fill value
				double scale() const noexcept {return _scale;}	//!< Accessor to scaling coefficient
				double offset() const noexcept {return _offset;}	//!< Accessor to offset of data
				BinaryGrid &parent() noexcept {return *_parent;}	//!< Accessor to the parent grid
				const BinaryGrid &parent() const noexcept {return *_parent;}	//!< Accessor to the parent grid

				/**
				 * \brief Set the parent grid of a variable
				 *
				 * This function sets the parent grid of the variable. It recreates the array of data accordingly. If the variable already held an array, it is freed.
				 * \param parent New parent of the variable
				 */
				void set_parent(BinaryGrid *parent);

				/**
				 * \brief Extract data from the grid
				 *
				 * This function returns the value of data at a particular point of the grid. It returns the integer value as it is stored, without applying the scaling coefficient and the offset.
				 * \param t Time index of the value in the grid
				 * \param x x-coordinate index of the value in the grid
				 * \param y y-coordinate index of the value in the grid
				 * \return Value at the given position as it is stored in the data structure (as an integer without application of the scale and offset)
				 */
				int value(int t,int x,int y) const noexcept {return _buffer[_parent->_sizexy*t+_parent->_sizex*y+x];}

				/**
				 * \brief Extract data from the grid
				 *
				 * This function returns the real value of data at a particular point of the grid. It retrieves the stored value and applies the scaling coefficient and the offset. For better performance, if several values must be retrieved and used in further calculations, consider also using Grid::value.
				 * \param t Time index of the value in the grid
				 * \param x x-coordinate index of the value in the grid
				 * \param y y-coordinate index of the value in the grid
				 * \return Value at the given position as it is stored in the data structure (as an integer without application of the scale and offset)
				 */
				double real_value(int t,int x,int y) const noexcept {return (double)value(t,x,y)*scale()+offset();}

				/**
				 * \brief Initializes the variable from a three-dimensional array
				 *
				 * This function initializes the object from a three-dimensional array given as argument. It automatically computes the appropriate scale value and offset to get the best accuracy of values for the grid using 16-bits signed integers to store them.
				 * \param dvalues Array of double values, with x-coordinates varying the fastest, then y-coordinates, and then time steps. NaN are used to indicate missing values in the array.
				 */
				void init_from_array(const double *dvalues);

				/**
				 * \brief Write the content of the variable to an output stream
				 *
				 * This function writes the content of the object to a binary output stream. The binary stream is portable (all variables are written with little-endian convention). The variable may be read with BinaryGrid::init.
				 * \param os Output stream. The stream must be ready to write.
				 */
				void write(std::ostream &os) const;

				/**
				 * \brief Write the content of the variable to an output stream in text format
				 *
				 * This function is the same as BinaryGrid::Variable::write but instead of writing to a binary stream, it writes data in a text format with column separated by tabulations.
				 * \param os Output stream. The stream must be ready to write.
				 */
				void write_text(std::ostream &os) const;

			private:
				BinaryGrid *_parent;	//!< Pointer to the parent grid
				int16_t _fillv;	//!< Fill value: an element which value is equal to the fill value does not have any value in fact. The fill value signals the absence of data.
				double _scale;	//!< Scaling coefficient. All values of the dataset must be multiplied by this coefficient
				double _offset;	//!< Offset. The offset must be added to all values in the dataset, after multiplication by the scaling coefficient
				int16_t *_buffer;	//!< Buffer used to store data from the file. The size of the buffer should be _sizex*_sizey*_steps. Data is stored with x-coordinates varying the fastest, then y-coordinates, and then time steps.
		};

		BinaryGrid():_vars(0) {}	//!< Standard constructor

		~BinaryGrid(); 	//!< Standard destructor

		/**
		 * \brief Copy constructor
		 *
		 * The copy constructor copies all members to the new structure.
		 *
		 * \param source Source object
		 */
		BinaryGrid(const BinaryGrid& source);

		/**
		 * \brief Move constructor
		 *
		 * The move constructor copies small members to the new object and transfers ownership of larger members.
		 *
		 * \param source Source object
		 */
		BinaryGrid(BinaryGrid&& source);

		/**
		 * \brief Constructor of a binary grid from its dimensions
		 *
		 * The constructor initializes the object and sets its capacity to the given arguments.
		 * \param nvars Number of variables
		 * \param sizex Width of the grid (number of cells in the x-dimension)
		 * \param sizey Hidth of the grid (number of cells in the y-dimension)
		 * \param steps Number of time steps
		 * \param xmin Lowest x-value (x-coordinate of the top-left corner of the grid)
		 * \param xstep Width of a cell
		 * \param ymin Lowest y-value (y-coordinate of the top-left corner of the grid)
		 * \param ystep Height of a cell
		 * \param tmin Start date as a number of days since 01/01/1950
		 * \param tstep Interval between two time steps in days (should be 1)
		 */
		BinaryGrid(int nvars,int sizex,int sizey,int steps,double xmin,double xstep,double ymin,double ystep,int tmin,int tstep);

		/**
		 * \brief Constructor of a binary grid from a file
		 *
		 * The constructor initializes the object and reads the grid from the file by calling the BinaryGrid::init function.
		 * \param file Path to the binary file
		 */
		explicit BinaryGrid(const char *file) {init(file);}

		/**
		 * \brief Return the index of a x-coordinate in the grid
		 *
		 * This function returns the index of the element corresponding to a specific value of the x-coordinate in the internal structure.
		 * The function returns the value of the greatest element smaller or equal to the given value. If no element is smaller, the function returns 0.
		 * \param x x-coordinate of the point
		 * \return Index of the cell in the x-dimension
		 */
		int get_xindex(double x) const noexcept;

		/**
		 * \brief Return the index of a y-coordinate in the grid
		 *
		 * This function returns the index of the element corresponding to a specific value of the y-coordinate in the internal structure.
		 * The function returns the value of the greatest element smaller or equal to the given value. If no element is smaller, the function returns 0.
		 * \param y y-coordinate of the point
		 * \return Index of the cell in the y-dimension
		 */
		int get_yindex(double y) const noexcept;

		/**
		 * \brief Return the index of a time value in the grid
		 *
		 * This function returns the index of the element corresponding to a specific value of the time coordinate in the internal structure.
		 * The function returns the value of the greatest element smaller or equal to the given value. If no element is smaller, the function returns 0.
		 * \param t Time value of the point
		 * \return Index of the cell in the time dimension
		 */
		int get_tindex(int t) const noexcept;

		/**
		 * \brief Initializes the grid from a binary file
		 *
		 * This function initializes the object from a binary file which path is given as argument. The file is fully read in memory.
		 * The file is a portable binary file where all values are written with the little-endian convention. The file holds exactly the following records in this order:
		 *  - Number of variables : 16-bits unsigned integer
		 * 	- Width of the grid (number of cells in the x-dimension): 16-bits unsigned integer
		 * 	- Height of the grid (number of cells in the y-dimension): 16-bits unsigned integer
		 * 	- Number of time steps: 32-bits unsigned integer
		 * 	- Lowest x-value (x-coordinate of the top-left corner of the grid): 64-bits double floating-point value
		 * 	- Width of a cell: 64-bits double floating-point value
		 * 	- Lowest y-value (y-coordinate of the top-left corner of the grid): 64-bits double floating-point value
		 * 	- Height of a cell: 64-bits double floating-point value
		 * 	- Start date as a number of days since 01/01/1950: 32-bits signed integer
		 * 	- Interval between two time steps in days (should be 1): 16-bits signed integer
		 * 	- _nvars variable, each one made of the following values:
		 * 		- Fill value (indicating void values in the dataset): 16-bits signed integer
		 * 		- Scale coefficient: 64-bits double floating-point value
		 * 		- Offset: 64-bits double floating-point value
		 *	 	- _sizex*_sizey*_steps 16-bits signed integer holding the values of the cells, with x-coordinates varying the fastest, then y-coordinates and then the time steps
		 *
		 * \param file Path to the binary file
		 */
		void init(const char *file);

		/**
		 * \brief Initializes the grid from a NetCDF file
		 *
		 * This function initializes the object from a NetCDF file which path is given as argument. Only the rectangle specified by the coordinates is extracted from the NetCDF file.
		 * \param file Path to the NetCDF file
		 * \param x1 Minimum x-coordinate of the extracted part
		 * \param y1 Minimum y-coordinate of the extracted part
		 * \param x2 Maximum x-coordinate of the extracted part
		 * \param y2 Maximum y-coordinate of the extracted part
		 */
		void init_from_netcdf(const char *file,double x1,double y1,double x2,double y2);

		/**
		 * \brief Write the content of the grid to a file
		 *
		 * This function writes the content of the object to a portable binary file which can be read with BinaryGrid::init. See also this function for the format of the file.
		 * \param file Path to the output file
		 */
		void write_to_file(const char *file) const;

		/**
		 * \brief Write the content of the grid to a text file
		 *
		 * This function writes the content of the object to a text file.
		 * \param file Path to the output file
		 */
		void write_to_text_file(const char *file) const;

		Variable &operator[](size_t i) const noexcept {return _vars[i];}	//!< Accessor to the i-th variable in the grid. Does not do bound-checking.
		int nvars() const noexcept {return _nvars;}	//!< Accessor to the number of variables in the dataset
		int sizex() const noexcept {return _sizex;}	//!< Accessor to the size of the dataset in the x-coordinate
		int sizey() const noexcept {return _sizey;}	//!< Accessor to the size of the dataset in the y-coordinate
		int steps() const noexcept {return _steps;}	//!< Accessor to the number of time steps
		double stepx() const noexcept {return _stepx;}	//!< Accessor to width of cell in x dimension
		double stepy() const noexcept {return _stepy;}	//!< Accessor to width of cell in y dimension
		double x(int i) const noexcept {return _startx+_stepx*i;}	//!< Return the x-coordinate associated with an index of the x-dimension in the grid
		double y(int i) const noexcept {return _starty+_stepy*i;}	//!< Return the y-coordinate associated with an index of the y-dimension in the grid
		int time(int i) const noexcept {return _start_time+_step_time*i;}	//!< Return the time associated with an index of the time dimension in the grid
	protected:
		int _nvars;	//!< Number of variables in the dataset
		int _sizex;	//!< Size of the dataset in the x-coordinate
		int _sizey;	//!< Size of the dataset in the y-coordinate
		int _sizexy;	//!< Product of width and height of grid
		int _steps;	//!< Size of the dataset of time steps
		double _startx;	//!< Start value of x-coordinates, or x-coordinate of the top left cell of the grid
		double _stepx;	//!< Width of a cell of the grid in the x dimension
		double _starty;	//!< Start value of y-coordinates, or y-coordinate of the top left cell of the grid
		double _stepy;	//!< Width of a cell of the grid in the y dimension
		int _start_time;	//!< Start value of time coordinates
		int _step_time;	//!< Length of a time step
		Variable *_vars;	//!< Array of variables stored in the grid
};

#endif   /* ----- #ifndef GRID_INC  ----- */
