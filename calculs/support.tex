\documentclass[10pt,a4paper,twocolumn]{article}
\usepackage[xetex]{graphicx}
\usepackage[libertine]{newtxmath}
\usepackage[no-math]{fontspec}
\usepackage{mathspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage[francais]{babel}
\usepackage[autolanguage]{numprint}
\usepackage{xcolor}
\usepackage{booktabs}
\usepackage{geometry}
\usepackage{array}
\usepackage{ifthen}
\usepackage{tikz}

\usetikzlibrary{arrows.meta}

\DeclareGraphicsExtensions{.pdf,.jpg,.png}   %%% standard extension for included graphics
\usepackage[xetex]{hyperref}
\hypersetup{
	pdfauthor   = {François Hissel},
	pdftitle    = {Note sur la transformation de Hilbert},
	pdfsubject  = {Note},
	pdfkeywords = {hilbert informatique raster},
	pdfcreator={XeTeX},
	pdfstartview={FitH},
	bookmarks=true,
	bookmarksnumbered=true,
	hypertexnames=false,
	breaklinks=true,
	colorlinks=true,
	linkcolor=blue,
	pdfborder={0 0 1},
	linkbordercolor={0 0 1}
}

\setmainfont[Mapping=tex-text]{Linux Libertine O}
\setsansfont[Mapping=tex-text]{Linux Biolinum O}

\geometry{a4paper,body={160mm,250mm},left=25mm,top=25mm,headheight=7mm,headsep=4mm}

\newdimen\HilbertLastX
\newdimen\HilbertLastY
\newcounter{HilbertOrder}

\def\DrawToNext#1#2{%
   \advance \HilbertLastX by #1
   \advance \HilbertLastY by #2
   \pgfpathlineto{\pgfqpoint{\HilbertLastX}{\HilbertLastY}}
   % Alternative implementation using plot streams:
   % \pgfplotstreampoint{\pgfqpoint{\HilbertLastX}{\HilbertLastY}}
}

% \Hilbert[right_x,right_y,left_x,left_x,up_x,up_y,down_x,down_y]
\def\Hilbert[#1,#2,#3,#4,#5,#6,#7,#8] {
  \ifnum\value{HilbertOrder} > 0%
     \addtocounter{HilbertOrder}{-1}
     \Hilbert[#5,#6,#7,#8,#1,#2,#3,#4]
     \DrawToNext {#1} {#2}
     \Hilbert[#1,#2,#3,#4,#5,#6,#7,#8]
     \DrawToNext {#5} {#6}
     \Hilbert[#1,#2,#3,#4,#5,#6,#7,#8]
     \DrawToNext {#3} {#4}
     \Hilbert[#7,#8,#5,#6,#3,#4,#1,#2]
     \addtocounter{HilbertOrder}{1}
  \fi
}

% \hilbert((x,y),order)
\def\hilbert((#1,#2),#3){%
   \advance \HilbertLastX by #1
   \advance \HilbertLastY by #2
   \pgfpathmoveto{\pgfqpoint{\HilbertLastX}{\HilbertLastY}}
   % Alternative implementation using plot streams:
   % \pgfplothandlerlineto
   % \pgfplotstreamstart
   % \pgfplotstreampoint{\pgfqpoint{\HilbertLastX}{\HilbertLastY}}
   \setcounter{HilbertOrder}{#3}
   \Hilbert[0mm,1mm,0mm,-1mm,1mm,0mm,-1mm,0mm]
   \pgfusepath{stroke}%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Tranformation de Hilbert}
\author{François Hissel}
\date{12 août 2015}

\begin{document}
\maketitle

\tikzset{element/.pic={\draw [arrows={-Latex[scale=0.5]}] (-0.1,-0.1) -- (-0.1,0.1) -- (0.1,0.1) -- (0.1,-0.1);}}

\section{Objectif}
La transformation de Hilbert est utilisée dans le logiciel pour représenter des sous-ensembles de raster sous une forme compacte. Elle constitue un homéomorphisme $\phi$ de $\left\{0,\cdots,2^n-1\right\}^2\mapsto\left\{0,\cdots,2^{2n}-1\right\}$ qui présente une bonne propriété de localité, c'est-à-dire que les images de deux points voisins dans la grille sont deux valeurs proches. Ceci permet de représenter un sous-ensemble connexe de $\left\{0,\cdots,2^n-1\right\}^2$ de façon particulièrement compacte, en ne conservant que les bornes des intervalles de l'image unidimensionnelle.

\section{Illustration}
Chaque cellule du raster est associée à un unique nombre entier, en suivant l'ordre dans lequel la courbe de Hilbert traverse les cellules.
La courbe de Hilbert est construite récursivement de la façon suivante :

\begin{center}
	\foreach \i in {0,1,...,5} {
		\pgfmathsetmacro{\j}{2^(-\i)}
		\pgfmathtruncatemacro{\k}{\i+1}
		\begin{tikzpicture}
			\draw [help lines,step=\j*1cm] (0,0) grid (2,2);
			\draw [ultra thick] (0,0) rectangle (2,2);
			\begin{scope}[xshift=\j*5mm,yshift=\j*5mm,scale=10*\j,color=red]
				\hilbert((0mm,0mm),\k);
			\end{scope}
			\node at (1,0) [below] {$n=\k$};
		\end{tikzpicture}
	}
\end{center}

\section{Transformation de coordonnées}
Le but de cette section est de caractériser l'homéomorphisme $\phi$ et sa réciproque $\phi^{-1}$.

\subsection{Transformation réciproque}
Soit un nombre $\alpha\in\left\{0,\cdots,2^{2n}-1\right\}$. On écrit $\alpha$ sous sa forme binaire :
\begin{equation}
	\alpha=\sum_{i=0}^{2n-1}\alpha_i 2^{2n-1-i}
	\label{eq_alpha}
\end{equation}
avec $\forall i\in \{0,\cdots,2n-1\},\alpha_i\in\{0,1\}$. L'écriture binaire de $\alpha$ est simplement $\alpha_0\alpha_1\cdots\alpha_{2n-1}$.

Soit aussi $(\beta,\gamma)=\phi^{-1}(\alpha)$, et de la même façon les $\beta_i$ et $\gamma_i$ sont les chiffres des écritures binaires de $\beta$ et $\gamma$ respectivement :
\begin{equation}
	\left\{
	\begin{aligned}
		& \beta=\sum_{i=0}^{n-1}\beta_i 2^{n-1-i} \\
		& \gamma=\sum_{i=0}^{n-1}\gamma_i 2^{n-1-i} \\
	\end{aligned}
	\right.
	\label{eq_beta_gamma}
\end{equation}

La signification des valeurs $\alpha$, $\beta$ et $\gamma$ figure sur le schéma suivant :

\begin{center}
	\begin{tikzpicture}
		\draw [fill=red!20!white] (2,0) rectangle (2.5,1.5);
		\draw [fill=red!20!white] (0,1.5) rectangle (2,2);
		\draw [help lines,step=0.5cm] (0,0) grid (3,3);
		\node at (2,1.5) [above right,outer sep=0mm,minimum width=0.5cm,minimum height=0.5cm,align=center,font=\small,fill=yellow!20!white,draw=black] {$\alpha$};
		\node at (2,0) [below right,outer sep=0mm,minimum width=0.5cm,align=center,font=\small] {$\beta$};
		\node at (0,1.5) [above left,outer sep=0mm,minimum height=0.5cm,font=\small] {$\gamma$};
		\node at (0,0) [below right,outer sep=0mm,minimum width=0.5cm,align=center,font=\small] {0};
		\node at (0.5,0) [below right,outer sep=0mm,minimum width=0.5cm,align=center,font=\small] {1};
		\node at (0,0) [above left,outer sep=0mm,minimum height=0.5cm,font=\small] {0};
		\node at (0,0.5) [above left,outer sep=0mm,minimum height=0.5cm,font=\small] {1};
	\end{tikzpicture}
\end{center}

Un algorithme de calcul de la transformation réciproque détermine les $\beta_i$ et $\gamma_i$ à partir des $\alpha_i$ pris deux à deux et d'un état dont la valeur initiale est 1, selon le tableau suivant :

\begin{center}
	\begin{tabular}{cccccc}
		\toprule
		\multicolumn{2}{c}{État} & \multicolumn{4}{c}{$\alpha_{2i}\alpha_{2i+1}$} \\
		\midrule
		& & 00 & 01 & 10 & 11 \\
		\midrule
		0 & \tikz{\pic [rotate=-90,xscale=-1] {element};} & 0,0,1 & 1,0,0 & 1,1,0 & 0,1,7 \\
		1 & \tikz{\pic {element};}                        & 0,0,0 & 0,1,1 & 1,1,1 & 1,0,6 \\
		2 & \tikz{\pic [rotate=90] {element};}            & 1,0,3 & 0,0,2 & 0,1,2 & 1,1,5 \\
		3 & \tikz{\pic [xscale=-1] {element};}            & 1,0,2 & 1,1,3 & 0,1,3 & 0,0,4 \\
		4 & \tikz{\pic [rotate=-90] {element};}           & 0,1,5 & 1,1,4 & 1,0,4 & 0,0,3 \\
		5 & \tikz{\pic [yscale=-1] {element};}            & 0,1,4 & 0,0,5 & 1,0,5 & 1,1,2 \\
		6 & \tikz{\pic [rotate=90,xscale=-1] {element};}  & 1,1,7 & 0,1,6 & 0,0,6 & 1,0,1 \\
		7 & \tikz{\pic [rotate=180] {element};}           & 1,1,6 & 1,0,7 & 0,0,7 & 0,1,0 \\
		\bottomrule
	\end{tabular}
\end{center}

Dans chaque cellule, les deux premiers chiffres correspondent aux valeurs de $\beta_i$ et $\gamma_i$, et le dernier est le nouvel état du système valable à l'itération suivante.

\subsection{Transformation directe}
Soient deux nombres $(\beta,\gamma)\in\left\{0,\cdots,2^n-1\right\}^2$, et leurs écritures binaires \eqref{eq_beta_gamma}. On cherche à calculer $\alpha=\phi(\beta,\gamma)$.

L'algorithme est similaire à celui de $\phi^{-1}$. On détermine les $\alpha_i$ deux à deux à partir des $\beta_i$ et $\gamma_i$ et d'un état dont la valeur initiale est 1. Le tableau de correspondance est le suivant :

\begin{center}
	\begin{tabular}{cccccc}
		\toprule
		\multicolumn{2}{c}{État} & \multicolumn{4}{c}{$(\beta_i,\gamma_i)$} \\
		\midrule
		& & (0,0) & (0,1) & (1,0) & (1,1) \\
		\midrule
		0 & \tikz{\pic [rotate=-90,xscale=-1] {element};} & 00,1 & 11,0 & 01,0 & 10,7 \\
		1 & \tikz{\pic {element};}                        & 00,0 & 01,1 & 11,1 & 10,6 \\
		2 & \tikz{\pic [rotate=90] {element};}            & 01,3 & 10,2 & 00,2 & 11,5 \\
		3 & \tikz{\pic [xscale=-1] {element};}            & 11,2 & 10,3 & 00,3 & 01,4 \\
		4 & \tikz{\pic [rotate=-90] {element};}           & 11,5 & 00,4 & 10,4 & 01,3 \\
		5 & \tikz{\pic [yscale=-1] {element};}            & 01,4 & 00,5 & 10,5 & 11,2 \\
		6 & \tikz{\pic [rotate=90,xscale=-1] {element};}  & 10,7 & 01,6 & 11,6 & 00,1 \\
		7 & \tikz{\pic [rotate=180] {element};}           & 10,6 & 11,7 & 01,7 & 00,0 \\
		\bottomrule
	\end{tabular}
\end{center}

Dans chaque cellule, les deux premiers chiffres correspondent aux valeurs de $\alpha_{2i}$ et $\alpha_{2i+1}$ respectivement, et le dernier est le nouvel état du système valable à l'itération suivante.

\end{document}


