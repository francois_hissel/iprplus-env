/*! \file iprplus-env.cpp
 * =====================================================================================
 *
 *       Filename:  iprplus-env.cpp
 *
 *    Description:  Calculation of environmental conditions for the estimation of IPR+
 *
 *        Version:  1.0
 *        Created:  11/08/2015 13:53:29
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  François Hissel (), francois.hissel@onema.fr
 *   Organization:  Onema
 *
 * =====================================================================================
 */

// Hack around stdlib bug with C++14.
//#include <initializer_list>  // force libstdc++ to include its config
//#undef _GLIBCXX_HAVE_GETS    // correct broken config
// End hack.
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#ifdef _WIN32
#define	EX_USAGE 64
#define	EX_IOERR 74
#else
#include <sysexits.h>
#endif
#include <exception>
#include <stdexcept>
#include <gdal_priv.h>
#include <ogr_spatialref.h>
#include "watershed.h"
#include "calculations.h"

using namespace std;

/**
 * \brief Return a full path for a file
 *
 * This function returns the full path for the file given as argument. If the path does not include a full directory specification, it is considered relative to the path of the executable. Otherwise, it is returned without any change.
 * \param execpath Absolute path of the executable (with the final trailing character)
 * \param path Original path (absolute or relative)
 * \return Full absolute path of the file
 */
string full_path(const string &execpath,const string &path) noexcept {
	if (path.length()==0) return "";
#if defined(_WIN32) || defined(_WIN64)
	if (path[0]=='\\' || (path.length()>1 && path[1]==':')) return path;
	return execpath+path;
#else
	if (path[0]=='/') return path;
	return execpath+path;
#endif
}

/**
 * \brief Display a short help message
 *
 * This function displays a short help message to describe the syntax of the command line.
 */
void display_help() {
	cout << "Syntax: iprplus-env [options]\n";
	cout << "Options:\n";
	cout << "\t-h      : Show this help message and exit.\n";
	cout << "\t-s file : Set path of GDAL raster file with directions of flow. By default srtm_dir.tif. See documentation for the format of the file.\n";
	cout << "\t-a file : Set path of the GDAL raster file with altitudes. The file is only used for temperature corrections. By default srtm_novoid.tif.\n";
	cout << "\t-c file : Set path of binary file with climate data. The file should hold at least 10 years of data before the year of calculation. By default data_climate.bin. See documentation for the expected format.\n";
 	cout << "\t-g file : Set path of the GDAL raster file with geology categories. By default geol.tif. See documentation for the expected format.\n";
 	cout << "\t-r file : Set path of the GDAL raster file with hydrological regimes. By default hydro.tif. See documentation for the expected format.\n";
	cout << "\t-d year : Set the year of the calculation. By default it is the year when the calculation is run. If 0, a third field is read on each line giving the year of the calculation.\n";
	cout << "\t-e epsg : Set the projection of the coordinates in the input file, by its EPSG code. If 0, no reprojection is done when matching data with NetCDF files (WGS84). Default is 0.\n";
	cout << "\t-f file : Set path of input data. By default, input data is read from standard input.\n";
}

/**
 * \brief Main program
 *
 * This function is the main running loop of the program. It reads arguments from the command line, input data, run the calculations and display the results.
 * The command line has the following syntax: <tt> iprplus-env [options] </tt>.
 * By default, input data is read from standard input and output is displayed on standard output.
 *
 * Options of the command line are chosen from the following list:
 * 	- <tt>-h</tt> : Display a short help message and exits.
 * 	- <tt>-s file</tt> : Set path of the GDAL raster file with flow directions. By default \a srtm_dir.tif.
 * 	- <tt>-a file</tt> : Set path of the GDAL raster file with altitudes. The file is only used for temperature corrections. By default \a srtm_novoid.tif.
 * 	- <tt>-c file</tt> : Set path of the binary file with climate data. The file should hold at least 10 years of data before the year of calculation. By default \a data_climate.bin.
 * 	- <tt>-g file</tt> : Set path of the GDAL raster file with geology categories. By default \a geol.tif.
 * 	- <tt>-r file</tt> : Set path of the GDAL raster file with hydrological regimes. By default \a hydro.tif.
 * 	- <tt>-d year</tt> : Set the year of the calculation. By default it is the year when the calculation is run. If 0, a third field is read on each line giving the year of the calculation.
 * 	- <tt>-e epsg</tt> : Set the projection of the coordinates in the input file, by its EPSG code. If 0, no reprojection is done when matching data with NetCDF files (WGS84). Default is 0.
 * 	- <tt>-f file</tt> : Set path of file with input data. If ommitted, input is read from standard input.
 *
 * 	See also \ref calldoc.
 * 
 * \param argc Number of arguments in the command line, including the path of the program itself
 * \param argv Array of the arguments of the command line
 * \return 0 if the program exitted normally, an error code otherwise
 */
int main(int argc,char **argv) {
	// Process command line arguments
	int i=1;
	bool args_end=false;
	string input_file="";
	string srtm_file="srtm_dir.tif";
	string clim_file="data_climate.bin";
	string geol_file="geol.tif";
	string hydro_file="hydro.tif";
	string alt_file="srtm_novoid.tif";
	int epsg=0;
	int date=-1;
	while (i<argc && !args_end) {
		if (argv[i][0]=='-') {
			switch (argv[i][1]) {
				case 'h':display_help();return 0;
				case 's':case 'c':case 'd':case 'f':case 'e':case 'a':case 'r':case 'g':
					if (i==argc-1) {
						display_help();
						return EX_USAGE;
					}
					if (argv[i][1]=='s') srtm_file=argv[i+1];
					else if (argv[i][1]=='a') alt_file=argv[i+1];
					else if (argv[i][1]=='c') clim_file=argv[i+1];
					else if (argv[i][1]=='g') geol_file=argv[i+1];
					else if (argv[i][1]=='r') hydro_file=argv[i+1];
					else if (argv[i][1]=='f') input_file=argv[i+1];
					else if (argv[i][1]=='d') {
						try {
							date=stoi(argv[i+1]);
						} catch (exception &e) {
							cerr << "Syntax error: incorrect date format " << argv[i+1] << ".\n";
						}
					}
					else if (argv[i][1]=='e') {
						try {
							epsg=stoi(argv[i+1]);
						} catch (exception &e) {
							cerr << "Syntax error: incorrect EPSG code " << argv[i+1] << ".\n";
						}
					}
					++i;
					break;
				default:
					cerr << "Syntax error: unknown option " << argv[i] << ".\n";
					display_help();
					return EX_USAGE;
			}
			++i;
		} else args_end=true;
	}
	if (date==-1) {
		time_t timt;
		time(&timt);
		struct tm *tim=localtime(&timt);
		date=tim->tm_year+1900;
	}
	// Get executable path
	string execpath=argv[0];
#if defined(_WIN32) || defined(_WIN64)
	execpath=execpath.substr(0,execpath.find_last_of('\\')+1);
#else
	execpath=execpath.substr(0,execpath.find_last_of('/')+1);
#endif
	// Open input stream
	ifstream ifs;
	if (input_file!="") {
		ifs.open(input_file);
		if (!ifs) {
			cerr << "Impossible to open input file " << input_file << ".\n";
			return EX_IOERR;
		}
		cin.rdbuf(ifs.rdbuf());
		cin.tie(0);
	} 
	// Open flow direction map
	GDALAllRegister();
	Raster rast;
	string path;
	try {
		path=full_path(execpath,srtm_file);
		rast.open(path);
	} catch (exception &e) {
		cerr << "Impossible to open flow direction raster file " << path << ".\n";
		return EX_IOERR;
	}
	// Open altitudes map
	Raster altitudes;
	if (alt_file!="") {
		try {
			path=full_path(execpath,alt_file);
			altitudes.open(path);
		} catch (exception &e) {
			cerr << "Impossible to open altitudes raster file " << path << ".\n";
			return EX_IOERR;
		}
	}
	// Open geological map
	Raster geology;
	if (geol_file!="") {
		try {
			path=full_path(execpath,geol_file);
			geology.open(path);
		} catch (exception &e) {
			cerr << "Impossible to open geological raster file " << path << ".\n";
			return EX_IOERR;
		}
	}
	// Open hydrological regimes map
	Raster hydrology;
	if (hydro_file!="") {
		try {
			path=full_path(execpath,hydro_file);
			hydrology.open(path);
		} catch (exception &e) {
			cerr << "Impossible to open hydrological regimes raster file " << path << ".\n";
			return EX_IOERR;
		}
	}
	// Open temperature data file
	BinaryGrid climate;
	try {
		path=full_path(execpath,clim_file);
		climate.init(path.c_str());
	} catch (exception &e) {
		cerr << "Impossible to open binary climate file " << path << ".\n";
		return EX_IOERR;
	}
	// Get or calculate mesh altitude data
	double *altitudes_mesh=0;
	if (alt_file!="") altitudes_mesh=get_altitude_mesh(altitudes,climate,(full_path(execpath,clim_file)+".alt").c_str());
	// Process input
	double x,y,oldx,oldy;
	double tsta,tbv,pbv,tampl,tjuly;
	string geol_mod,hydro_mod;
	OGRCoordinateTransformation *trans=0;
	if (epsg!=0) {
		OGRSpatialReference src,dest;
		src.importFromEPSG(epsg);
		dest.importFromEPSG(4326);
#if GDAL_MAJOR_VERSION >= 3
		dest.SetAxisMappingStrategy(OAMS_TRADITIONAL_GIS_ORDER);
#endif
		trans=OGRCreateCoordinateTransformation(&src,&dest);
	}
	string line;
	int calyear=date;
	RasterSubset ss(rast.get_dataset());
	Coverage coverage;
	cout << "x\ty\tDate\tSBV\tTEMP_MEAN_STA\tTEMP_AMPL_STA\tTEMP_MEAN_BV\tPREC_MEAN_BV\tTEMP_MEAN_STA_07\tGEOL\tREG4_GV" << endl;
	while (!cin.eof()) {
		getline(cin,line);	
		if (line.size()!=0) {
			istringstream iss(line);
			iss.exceptions(ifstream::failbit | ifstream::badbit);
			try {
				oldx=x;
				oldy=y;
				iss >> x >> y;
				if (date==0) iss >> calyear;
				if (calyear-10<climate.time(0) || calyear-1>climate.time(climate.steps()-1)) throw exception();
				if (trans!=0) trans->Transform(1,&x,&y);
				if (!coverage.initialized() || (oldx-x)*(oldx-x)+(oldy-y)*(oldy-y)>1e-8) {
					ss=rast.watershed(x,y,5e-3);
					coverage.init(&climate,&ss);
				} 
				//ss.save("test.tif");
				tsta=average_point(climate[0],x,y,calyear-10,calyear-1,(alt_file=="")?0:&altitudes,altitudes_mesh);
				//psta=prec_point(&ncprec,x,y,calyear-10,calyear-1);
				tbv=average_watershed(climate[0],coverage,calyear-10,calyear-1);
				pbv=average_watershed(climate[1],coverage,calyear-10,calyear-1);
				tjuly=average_point(climate[8],x,y,calyear-10,calyear-1,(alt_file=="")?0:&altitudes,altitudes_mesh);
				tampl=amplitude(climate,2,x,y,calyear-10,calyear-1);
				geol_mod="";
				if (geology.get_dataset()!=0) {
					int mod=get_raster_int_value(geology,x,y);
					geol_mod=mod==0?"":geol_modal[mod-1];
				}
				hydro_mod="";
				if (hydrology.get_dataset()!=0) {
					int mod=get_raster_int_value(hydrology,x,y);
					hydro_mod=mod==0?"":hydro_modal[mod-1];
				}
				size_t pos=iss.rdbuf()->pubseekoff(0,ios_base::cur,ios_base::in);
				if (pos<line.length()) {
					if (pos>0 && (line[pos]=='\n' || line[pos]=='\t')) --pos;
					line.erase(pos);
				}
				cout << line << '\t' << ss.area() << '\t' << tsta << '\t' << tampl << '\t' << tbv << '\t' << pbv << '\t' << tjuly << '\t' << geol_mod << '\t' << hydro_mod << endl;
			} catch (exception &e) {
				cout << line << "\t-- Calculation error" << endl;
			}
		}
	}
	if (trans!=0) delete trans;
	delete[] altitudes_mesh;
	// Return and exit
	return 0;
}

//int mainb(int argc,char **argv) {
//	GDALAllRegister();
//	Raster rast;
//	rast.open("srtm_ex.tif");
//	clock_t t=clock();
//	RasterSubset ss=rast.watershed(3,15);
//	cout << ss << endl;
//	cout << (clock()-t) << " ticks" << endl;
//	size_t i=0;
//	t=clock();
//	for (RasterSubset::HorizontalIterator it=ss.hbegin();it!=ss.hend();++it) {
//		cout << ((*it)?1:0);
//		if (++i==27) {
//			cout << endl;
//			i=0;
//		}
//	}
//	cout << endl;
//	cout << (clock()-t) << " ticks" << endl;
//	i=0;
//	t=clock();
//	for (RasterSubset::HorizontalIterator it=ss.hrbegin();it!=ss.hrend();--it) {
//		cout << ((*it)?1:0);
//		if (++i==27) {
//			cout << endl;
//			i=0;
//		}
//	}
//	cout << endl;
//	cout << (clock()-t) << " ticks" << endl;
//	i=0;
//	t=clock();
//	for (RasterSubset::VerticalIterator it=ss.vbegin();it!=ss.vend();++it) {
//		cout << ((*it)?1:0);
//		if (++i==16) {
//			cout << endl;
//			i=0;
//		}
//	}
//	cout << endl;
//	cout << (clock()-t) << " ticks" << endl;
//	i=0;
//	t=clock();
//	for (RasterSubset::VerticalIterator it=ss.vrbegin();it!=ss.vrend();--it) {
//		cout << ((*it)?1:0);
//		if (++i==16) {
//			cout << endl;
//			i=0;
//		}
//	}
//	cout << endl;
//	cout << (clock()-t) << " ticks" << endl;
//	return 0;
//}
