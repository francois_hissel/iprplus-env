\documentclass[10pt,a4paper]{article}
\usepackage[xetex]{graphicx}
\usepackage{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage[francais]{babel}
\usepackage{tikz}
\usepackage{fancyhdr}
\usepackage{array}
\usepackage{listings}
\usepackage{onema}

\DeclareGraphicsExtensions{.pdf,.jpg,.png}   %%% standard extension for included graphics
\usepackage[xetex]{hyperref}
\hypersetup{
	pdfauthor   = {François Hissel},
	pdftitle    = {Documentation de l'outil de calcul des données environnementales},
	pdfsubject  = {SEEE},
	pdfkeywords = {sie seee},
	pdfcreator={XeTeX},
	pdfstartview={FitH},
	bookmarks=true,
	bookmarksnumbered=true,
	hypertexnames=false,
	breaklinks=true,
	colorlinks=true,
	linkcolor=blue,
	%pdfborder={0 0 1},
	linkbordercolor={0 0 1}
}

\setmainfont[Mapping=tex-text,Numbers=OldStyle]{Linux Libertine O}
\setsansfont[Mapping=tex-text,Numbers=OldStyle]{Linux Biolinum O}

\usepackage[framemethod=tikz,skipbelow=\topskip,skipabove=\topskip]{mdframed}
\lstset{abovecaptionskip=0pt,belowcaptionskip =0pt,framextopmargin=-\topsep}
\definecolor{bggray}{rgb}{0.85, 0.85, 0.85}
\mdfsetup{leftmargin=0pt,rightmargin=0pt,backgroundcolor=bggray,middlelinecolor=black,roundcorner=10}
\usepackage{etoolbox}
\BeforeBeginEnvironment{lstlisting}{\begin{mdframed}\vspace{-0.7em}}
\AfterEndEnvironment{lstlisting}{\vspace{-0.5em}\end{mdframed}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Outil de calcul des données environnementales}
\author{Onema}
\date{11 février 2016}
\notedescription{Manuel d'utilisation}{}{1.1}{}{}

\begin{document}
\maketitle

\section{Installation}
Lancer le programme \texttt{setup.exe}. L'installation nécessite les droits d'administrateur.

\section{Exécution}
Le programme ne dispose pas d'interface graphique. Il se lance exclusivement en ligne de commandes. Pour l'exécuter, ouvrir une invite de commandes, puis taper la commande suivante :

\begin{lstlisting}[language=bash]
iprplus-env [options]
\end{lstlisting}

\texttt{[options]} est une succession d'un ou plusieurs des groupes suivants :

\begin{tabular}{>{\ttfamily}p{0.14\linewidth}p{0.77\linewidth}}
	-h & Affiche un rappel de la syntaxe et quitte le programme. \\
	-s \textit{fichier} & Définit le chemin du fichier contenant le plan de drainage. Par défaut \texttt{srtm\_dir.tif}. Voir aussi \ref{par:drainage} pour plus d'informations sur les exigences du fichier. \\
	-a \textit{fichier} & Définit le chemin du fichier contenant les altitudes. Ce fichier n'est utilisé que pour calculer les corrections de température à la station dues à l'altitude par rapport à l'altitute moyenne de la maille. Par défaut \texttt{srtm\_novoid.tif}. Le fichier doit être une couche Raster supportée par GDAL 1.11, qui couvre l'ensemble des bassins versants des stations pour lesquelles le calcul est demandé. Chaque cellule contient la valeur en mètres de l'altitude. \\
	-c \textit{fichier} & Définit le chemin du fichier contenant les données climatiques (températures et précipitations). Le fichier doit contenir a minima les 10 ans de données précédant la date de l'opération de pêche, sans quoi le calcul échouera. Par défaut \texttt{data\_climate.bin}. Voir aussi \ref{par:binaire} pour plus d'informations sur les exigences du fichier. \\
	-g \textit{fichier} & Définit le chemin du fichier contenant les catégories géologiques. Par défaut \texttt{geol.tif}. Le fichier doit être une couche Raster supportée par GDAL 1.11, qui couvre l'ensemble des bassins versants des stations pour lesquelles le calcul est demandé. Voir aussi \ref{par:geol} pour plus d'informations sur les exigences du fichier. \\
	-h \textit{fichier} & Définit le chemin du fichier contenant les régimes hydrologiques. Par défaut \texttt{hydro.tif}. Le fichier doit être une couche Raster supportée par GDAL 1.11, qui couvre l'ensemble des bassins versants des stations pour lesquelles le calcul est demandé. Voir aussi \ref{par:hydro} pour plus d'informations sur les exigences du fichier. \\
	-d \textit{annee} & Définit l'année de calcul. Par défaut, il s'agit de l'année courante (lorsque le calcul est lancé). Si \texttt{annee} vaut 0, la troisième colonne du fichier d'entrée est utilisée comme année de calcul (et peut donc être différente pour chaque opération). \\
	-e \textit{epsg} & Définit la projection des coordonnées $x$ et $y$ du fichier d'entrée, à partir de son code EPSG. Si \texttt{epsg} vaut 0, aucune reprojection n'est réalisée, et le programme considère que les coordonnées du fichier d'entrée sont exprimées dans le même système de projection que les fichiers supports contenant les températures et précipitations (WGS84). Par défaut, 0. Quelques codes EPSG pertinents sont les suivants :
	\begin{itemize}
		\item 2154 : Lambert 93
		\item 27572 : Lambert 2 étendu
		\item 4326 : WGS84
	\end{itemize}
	\\
	-f \textit{fichier} & Définit le chemin du fichier des données d'entrée. Voir aussi \ref{par:entree} pour le format de ce fichier. \\
\end{tabular}

Si l'option \texttt{-f} n'est pas présente dans la ligne de commande, le programme attend ses données depuis l'entrée standard.

Les résultats sont écrits sur la sortie standard et peuvent être redirigés vers un fichier de sortie en ajoutant "\texttt{> \textit{fichier}}" à la fin de la ligne de commande, où \texttt{fichier} est le chemin du fichier où écrire les résultats. Voir aussi \ref{par:sortie} pour la description du format des données de sortie.

Exemple de ligne de commande :

\begin{lstlisting}[language=bash,basicstyle=\footnotesize,showtabs=true]
iprplus-env -e 27572 -d 0 -c data_climate.bin -f input.csv >output.csv
\end{lstlisting}

\section{Format des fichiers}

\subsection{Fichier d'entrée}
\label{par:entree}
Le fichier d'entrée est un fichier texte. Chaque ligne correspond à une opération, caractérisée par des coordonnées $x$ et $y$ et une année. Les valeurs sont séparées par des tabulations. Les valeurs décimales sont représentées en utilisant un point comme séparateur décimal. L'année ne doit être précisée que si l'option \texttt{-d} est utilisée.

Lorsque plusieurs opérations figurent dans le fichier d'entrée, il est fortement recommandé de les trier de telle sorte que les opérations correspondant à une même station se suivent dans le fichier. Ceci permet au programme de retenir l'extension du bassin versant entre deux opérations, et d'éviter de recommencer le calcul de cette opération, l'une des plus longues dans l'algorithme. Le calcul peut en être fortement accéléré.

Exemple de fichier d'entrée :

\begin{lstlisting}[showtabs=true]
600414	2460950	1998
600414	2460950	1998
604489	2461384	1998
587520	2459481	1998
\end{lstlisting}

\subsection{Fichier de sortie}
\label{par:sortie}
Le fichier de sortie est un fichier texte. Chaque ligne correspond à une opération, et les lignes figurent dans le même ordre que dans le fichier d'entrée. Les valeurs sont séparées par des tabulations. Les valeurs décimales sont représentées en utilisant un point comme séparateur décimal.

Chaque ligne contient les données suivantes, dans l'ordre des colonnes :
\begin{itemize}
	\item abscisse du point (rappel de l'entrée),
	\item ordonnée du point (rappel de l'entrée),
	\item date (si présente dans le fichier d'entrée),
	\item surface du bassin versant (en km²),
	\item température moyenne à la station pendant les 10 années civiles complètes précédant l'année de pêche (en °C),
	\item amplitude thermique moyenne à la station pendant les 10 années civiles complètes précédant l'opération de pêche (moyenne des différences entre les températures moyennes des mois les plus chaud et plus froid de l'année) (en °C),
	\item température moyenne sur le bassin versant pendant les 10 années civiles complètes précédant l'année de pêche (en °C),
	\item précipitations annuelles cumulées moyennes sur le bassin versant pendant les 10 années civiles complètes précédant l'opération de pêche (en mm),
	\item température moyenne à la station pendant les mois de juillet des 10 années civiles complètes précédant l'opération de pêche (en °C),
	\item modalité du substrat géologique dominant à la station (\texttt{c} pour calcaire, \texttt{s} pour siliceux),
	\item modalité du régime hydrologique à la station :
		\begin{itemize}
			\item \texttt{reg\_ng} : nival ou nivo-glaciaire,
			\item \texttt{reg\_pn} : pluvial abondant ou faible influence nivale,
			\item \texttt{reg\_pm} : océanique irrégulier ou influence méditerranéenne,
			\item \texttt{reg\_pf} : pluvial océanique.
		\end{itemize}
\end{itemize}

Exemple de fichier de sortie (correspondant au fichier d'entrée précédent et à la ligne de commande précédente) :

\begin{lstlisting}[basicstyle=\footnotesize,showtabs=true]
600414	2460950	1998	15969.8	11.1869	14.5962	10.5728	742.48	18.9625	c	reg_pm
600414	2460950	1998	15969.8	11.1869	14.5962	10.5728	742.48	18.9625	c	reg_pm
604489	2461384	1998	128.513	11.1992	14.6764	10.8504	681.526	19.0156	c	reg_pm
587520	2459481	1998	102.001	11.0634	14.5962	10.7206	719.767	18.839	c	reg_pm
\end{lstlisting}

\subsection{Exigences du plan de drainage}
\label{par:drainage}
Le plan de drainage est une couche SIG raster qui doit pouvoir être lue par GDAL 1.11. (En particulier le format Tiff est accepté.) La couche doit couvrir l'ensemble du domaine constitué des stations du fichier d'entrée et de leurs bassins versants amont.

Chaque cellule du raster donne la direction du flux hydraulique. Elle pointe vers la direction de la plus forte pente, après suppression des fosses. La couche est supposée être fournie dans le système WGS84 (latitude, longitude). Une cellule prend l'une des valeurs entières suivantes, selon la direction de la plus forte pente :
\begin{itemize}
	\item 0 : pas de flux
	\item 1 : est
	\item 2 : sud-est
	\item 4 : sud
	\item 8 : sud-ouest
	\item 16 : ouest
	\item 32 : nord-ouest
	\item 64 : nord
	\item 128 : nord-est
\end{itemize}

Une valeur inférieure à 0 représente une cellule vide ou une erreur dans le couche. Elle n'est pas prise en compte dans le calcul du bassin versant.

\subsection{Exigences du fichier climatique}
\label{par:binaire}
Le fichier climatique est un fichiers binaire. Il répond aux contraintes suivantes :
\begin{itemize}
	\item Sa couverture recouvre toute la couche du plan de drainage.
	\item Ses coordonnées sont exprimées dans le système de coordonnées WGS84, en degrés.
	\item Les température et précipitation sont données à une fréquence journalière.
	\item La température est donnée en degrés Celsius.
	\item La précipitation journalière cumulée est donnée en mm.
	\item Les grilles de température et de précipitation sont identiques.
\end{itemize}

Toutes les données du fichier binaire sont écrites avec la convention \textit{little-endian} (octet de poids faible au début). Les données successives sont les suivantes :
\begin{itemize}
	\item Nombre de variables : entier non signé de 16 bits (toujours 14)
	\item Largeur de la grille : entier non signé de 16 bits
	\item Hauteur de la grille : entier non signé de 16 bits
	\item Nombre de pas de temps : entier non signé de 32 bits
	\item Longitude minimale : nombre flottant de 64 bits
	\item Largeur d'une cellule (en degrés) : nombre flottant de 64 bits
	\item Latitude minimale : nombre flottant de 64 bits
	\item Hauteur d'une cellule (en degrés) : nombre flottant de 64 bits
	\item Date de début (en nombre de jours depuis le 01/01/1950) : entier signé de 32 bits
	\item Intervalle entre deux pas de temps (devrait toujours valoir 1) : entier signé de 16 bits
	\item Puis, pour chaque variable, la liste des valeurs suivantes :
		\begin{itemize}
			\item Valeur d'absence : entier signé de 16 bits correspondant à l'absence de valeur dans la grille ultérieure
			\item Coefficient d'échelle : nombre flottant de 64 bits. Les valeurs de la grille doivent être multipliées par le coefficient d'échelle.
			\item Décalage : nombre flottant de 64 bits. Après multiplication par le coefficient d'échelle, les valeurs de la grille doivent être ajoutées au coefficient d'échelle pour obtenir la valeur réelle de température ou de précipitation.
			\item \texttt{Largeur x longueur x nombre de pas de temps} valeurs correspondant aux valeurs des cellules, en faisant varier d'abord la longitude, puis la latitude, puis le pas de temps : entiers signés de 16 bits
		\end{itemize}
\end{itemize}

Le fichier doit contenir exactement les 14 variables suivantes, au pas de temps annuel :
\begin{itemize}
	\item Moyenne annuelle de la température (°C)
	\item Cumul annuel des précipitations (mm)
	\item 12 valeurs correspondant aux moyennes mensuelles des températures (°C)
\end{itemize}

\subsection{Exigences sur la couche des substrats géologiques}
\label{par:geol}
La couche des substrats géologiques est une couche SIG raster qui doit pouvoir être lue par GDAL 1.11. (En particulier le format Tiff est accepté.) La couche doit couvrir l'ensemble du domaine constitué des stations du fichier d'entrée et de leurs bassins versants amont.

La couche contient pour chaque cellule le substrat géologique dominant, représenté par un nombre entier selon deux modalités :
\begin{itemize}
	\item 1 : siliceux
	\item 2 : calcaire
\end{itemize}

La valeur 0 indique l'absence de valeur. Cela ne provoque pas d'erreur de calcul, mais aucun résultat ne sera retourné pour les stations situées dans une telle cellule.

\subsection{Exigences sur la couche des régimes hydrologiques}
\label{par:hydro}
La couche des régimes hydrologiques est une couche SIG raster qui doit pouvoir être lue par GDAL 1.11. (En particulier le format Tiff est accepté.) La couche doit couvrir l'ensemble du domaine constitué des stations du fichier d'entrée et de leurs bassins versants amont.

La couche contient pour chaque cellule le régime hydrologique, représenté par un nombre entier selon quatre modalités :
\begin{itemize}
		\item 1 : nival ou nivo-glaciaire,
		\item 2 : pluvial abondant ou faible influence nivale,
		\item 3 : océanique irrégulier ou influence méditerranéenne,
		\item 4 : pluvial océanique.
\end{itemize}

La valeur 0 indique l'absence de valeur. Cela ne provoque pas d'erreur de calcul, mais aucun résultat ne sera retourné pour les stations situées dans une telle cellule.

\end{document}

